INSERT INTO users (user_id, master_role, active, status_id, first_name, last_name, password, email, phone_number)
VALUES (1, 1, 1, 1, 'Jeffery', 'Brannon', '$2a$10$7dHcvwnvGN1alBQzEjiOzuo.0JFPlsskYsAODVU.5JsotprqyHJPm', 'jlb1976@missouristate.edu', '417-423-9219');

INSERT INTO users (user_id, master_role, active, status_id, first_name, last_name, password, email, phone_number)
VALUES (2, 2, 1, 1, 'Student', 'Smith', '$2a$10$leU7Rb1l2qwjwmftEenzIeFX6yqpA1OY/eqSs9lEAliIjbClnTznq', 'student', '417-555-1212');

INSERT INTO users (user_id, master_role, active, status_id, first_name, last_name, password, email, phone_number)
VALUES (3, 1, 1, 1, 'Approver', 'Smith', '$2a$10$leU7Rb1l2qwjwmftEenzIeFX6yqpA1OY/eqSs9lEAliIjbClnTznq', 'approver', '417-555-1212');

INSERT INTO users (user_id, master_role, active, status_id, first_name, last_name, password, email, phone_number)
VALUES (4, 1, 1, 1, 'Manager', 'Smith', '$2a$10$leU7Rb1l2qwjwmftEenzIeFX6yqpA1OY/eqSs9lEAliIjbClnTznq', 'manager', '417-555-1212');

INSERT INTO users (user_id, master_role, active, status_id, first_name, last_name, password, email, phone_number)
VALUES (5, 1, 1, 1, 'Director', 'Smith', '$2a$10$leU7Rb1l2qwjwmftEenzIeFX6yqpA1OY/eqSs9lEAliIjbClnTznq', 'director', '417-555-1212');

