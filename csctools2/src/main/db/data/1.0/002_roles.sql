INSERT INTO roles (role_id, role, role_viewable, description)
VALUES
 (1,'ROLE_ADMIN', 'Administrator', 'Master Role Admin')
,(2,'ROLE_STUDENT', 'Student', 'Master Role Student')
,(3,'ROLE_APPADMIN', 'Application Admin', 'Administrates the Web Application')
,(4,'ROLE_CR_DIRECTOR', 'Course Request Director', 'Can approve, grant, or deny request')
,(5,'ROLE_CR_MANAGER', 'Course Request Manager', 'Can grant, deny, or place request on hold')
,(6,'ROLE_CR_APPROVER', 'Course Request Approver', 'Can approve, deny, or place request on hold');