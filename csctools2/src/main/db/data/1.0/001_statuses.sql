INSERT INTO statuses (status_id, status_literal, status_viewable)
VALUES 
 (1, 'ACTIVE', 'Active')
,(2, 'INACTIVE', 'Inactive')
,(3, 'DELETED', 'Deleted')
,(4, 'PENDING', 'Pending')
;