INSERT INTO user_roles (user_id, role_id)
VALUES (1,3); -- add app admin role for user

INSERT INTO user_roles (user_id, role_id)
VALUES (3,6); -- add cr_approver role for approver user

INSERT INTO user_roles (user_id, role_id)
VALUES (4,5); -- add cr_manager role for manager user

INSERT INTO user_roles (user_id, role_id)
VALUES (5,5); -- add cr_director role for director user

INSERT INTO user_roles(user_id, role_id)
VALUES(1,1);  -- add admin role for jefferybrannon@gmail.com user

INSERT INTO user_roles(user_id, role_id)
VALUES(2,2);  -- add student role for student user
  
INSERT INTO user_roles(user_id, role_id)
VALUES(3,1);  -- add admin role for approver user

INSERT INTO user_roles(user_id, role_id)
VALUES(4,1);  -- add admin role for manager user

INSERT INTO user_roles(user_id, role_id)
VALUES(5,1);  -- add admin role for director user