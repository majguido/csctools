CREATE TABLE courses (
  course_id int NOT NULL AUTO_INCREMENT,
  course_section VARCHAR(12) NOT NULL,
  course_title VARCHAR(256) NOT NULL,
  course_crn int,
  course_description VARCHAR(2048), 
  status_id int DEFAULT '1',
--  created_by varchar(64) NOT NULL DEFAULT 'SYSADMIN',
--  created_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
--  modified_by varchar(64) NOT NULL DEFAULT 'SYSADMIN',
--  modified_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
--  sys_mod_count int NOT NULL DEFAULT '0',
  PRIMARY KEY (course_id),
  UNIQUE KEY uc_courses_sectiontitle (course_section, course_title)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

