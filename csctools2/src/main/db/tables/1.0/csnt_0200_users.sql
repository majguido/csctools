CREATE TABLE users (
  user_id int NOT NULL AUTO_INCREMENT,
  master_role int NOT NULL,
  active int DEFAULT '0',
  status_id int DEFAULT '4',
  first_name varchar(255) NOT NULL,
  last_name varchar(255) NOT NULL,
  password varchar(255) NOT NULL,
  email varchar(255) NOT NULL,
  phone_number varchar(12),
  created_by varchar(64) NOT NULL DEFAULT 'SYSADMIN',
  created_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modified_by varchar(64) NOT NULL DEFAULT 'SYSADMIN',
  modified_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  sys_mod_count int NOT NULL DEFAULT '0',
  PRIMARY KEY (user_id),
  UNIQUE KEY uc_users_email (email)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
