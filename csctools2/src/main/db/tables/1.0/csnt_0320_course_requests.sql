CREATE TABLE courserequests (
	course_request_id int NOT NULL AUTO_INCREMENT,
	status_id int DEFAULT '4',
	request_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	course VARCHAR(256) NOT NULL,
	first_name VARCHAR(256) NOT NULL,
	last_name VARCHAR(256) NOT NULL,
	email VARCHAR(256) NOT NULL,
	semester VARCHAR(64) NOT NULL,
	`year`int NOT NULL,
	m_number VARCHAR(32) NOT NULL,
	student_notes VARCHAR(4000),
	CONSTRAINT pk_courserequestid PRIMARY KEY(course_request_id)
);