CREATE TABLE user_roles (
  user_id int NOT NULL,
  role_id int NOT NULL,
  created_by varchar(64) NOT NULL DEFAULT 'SYSADMIN',
  created_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modified_by varchar(64) NOT NULL DEFAULT 'SYSADMIN',
  modified_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  sys_mod_count int NOT NULL DEFAULT '0',
  PRIMARY KEY (user_id,role_id),
  UNIQUE KEY uc_role_userid_roleid (user_id,role_id),
  CONSTRAINT FK_roleid_role_roleid FOREIGN KEY (role_id) REFERENCES roles (role_id),
  CONSTRAINT FK_userid_user_userid FOREIGN KEY (user_id) REFERENCES users (user_id),
  CONSTRAINT fk_userroles_roleid FOREIGN KEY (role_id) REFERENCES roles (role_id),
  CONSTRAINT fk_userroles_userid FOREIGN KEY (user_id) REFERENCES users (user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
