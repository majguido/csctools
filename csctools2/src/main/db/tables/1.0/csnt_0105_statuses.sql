CREATE TABLE statuses (
  status_id int NOT NULL,
  status_literal varchar(32) NOT NULL,
  status_viewable varchar(32) NOT NULL,
  created_by varchar(64) NOT NULL DEFAULT 'SYSADMIN',
  created_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modified_by varchar(64) NOT NULL DEFAULT 'SYSADMIN',
  modified_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  sys_mod_count int NOT NULL DEFAULT '0',
  PRIMARY KEY (status_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
