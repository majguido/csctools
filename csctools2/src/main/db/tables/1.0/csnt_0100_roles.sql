CREATE TABLE roles (
  role_id int NOT NULL,
  role varchar(255) DEFAULT NULL,
  role_viewable varchar(255) DEFAULT NULL,
  description varchar(1024) DEFAULT NULL,
  created_by varchar(64) NOT NULL DEFAULT 'SYSADMIN',
  created_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modified_by varchar(64) NOT NULL DEFAULT 'SYSADMIN',
  modified_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  sys_mod_count int NOT NULL DEFAULT '0',
  PRIMARY KEY (role_id),
  UNIQUE KEY uc_role_roleid (role_id,role),
  UNIQUE KEY uc_role_role (role)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
