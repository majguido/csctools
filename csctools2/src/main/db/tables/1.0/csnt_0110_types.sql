CREATE TABLE types (
  type_id int NOT NULL,
  type_literal varchar(32) NOT NULL,
  type_viewable varchar(32) NOT NULL,
  created_by varchar(64) NOT NULL DEFAULT 'SYSADMIN',
  created_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  modified_by varchar(64) NOT NULL DEFAULT 'SYSADMIN',
  modified_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  sys_mod_count int NOT NULL DEFAULT '0',
  PRIMARY KEY (type_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
