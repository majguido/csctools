

/**
 *  Validate Email
 */
const validateEmail = (email) => {
  return email.match(
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  );
};


/**
 *   Validate Phone Number
 */
const validatePhoneNumber = (phoneNumber) => {
	//var pattern = /^[0-9]{10}/;
	//var pattern = "/^(\d{3}[-]?){1,2}(\d{4})$/;
	var pattern = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
	return phoneNumber.match(pattern);
};

/**
 *  QID = Get Element By ID
 */
function qid(id) {
	return document.getElementById(id);
}

/**
 *  String Has Text
 */
function stringHasText(obj) {
	//if (obj instanceof String) {
		if (obj != null && obj != undefined && obj.length > 0) {
			return true;
		}
		
	//	return false;	
	//}
	
	return false;
}

/**
 *  Elements Empty
 *  This method only takes elements that contain
 *  the property "value"
 */
function elementsEmpty(arrayOfElements) {
	let response = false;
	
	try {
		for (var i=0; i < arrayOfElements.length; i++) {
			let value = arrayOfElements[i].value;
			
			if (value == undefined || value == null || value.length == 0) {
				response = true;
				break;
			} 
		}
	} catch(err) {
		console.error(err);
	}
	
	return response;
}

/**
 *  Add Remove Boolean Attribute
 */
function addRemoveBooleanAttribute(action, attribute, arrayOfElements) {
	try {
		if (action == "add" || action == "remove") {
			arrayOfElements.forEach((element)=>{
				if (action == "add") {
					element.setAttribute(attribute,'');
				} else if (action == "remove") {
					element.removeAttribute(attribute);
				}
			});
		}
	} catch(err) {
		console.error(err);
	}
}

/**
 *  Toast
 */
function toast(type, heading, message) {
	let toastContainer = qid("toastContainer");
	let toastHeader = qid("toastHeader");
	let toastImage = qid("toastImage");
	let toastHeading = qid("toastHeading");
	let toastTiming = qid("toastTiming");
	let toastBody = document.getElementsByClassName("toast-body")[0];
	
	// Remove all header classes and all header image classes
	toastHeader.className = "";
	toastImage.className = "";
	
	// Change the background color and heading
	switch(type) {
		case "success":
			toastImage.classList.add("fa-solid","fa-circle-check", "text-white");
			toastHeading.innerHTML = (heading != undefined && heading.length > 0) ? heading : "Success";
			toastHeader.classList.add("toast-header","bg-success", "toast");		
		    break;
		case "danger":
			toastImage.classList.add("fa-solid","fa-circle-exclamation", "text-white");
			toastHeading.innerHTML = (heading != undefined && heading.length > 0) ? heading : "Error";
			toastHeader.classList.add("toast-header","bg-danger", "toast");		
		    break;
		case "warning":
			toastImage.classList.add("fa-solid","fa-triangle-exclamation", "text-white");	
			toastHeading.innerHTML = (heading != undefined && heading.length > 0) ? heading : "Warning";
			toastHeader.classList.add("toast-header","bg-warning", "toast");		
		    break;
		case "info":
			toastImage.classList.add("fa-solid", "fa-circle-info","text-white");
			toastHeading.innerHTML = (heading != undefined && heading.length > 0) ? heading : "Information";
			toastHeader.classList.add("toast-header","bg-info", "toast");		
		    break;
	}
	
	// Set the message
	toastBody.innerHTML = message;
		
	// Configure the toast
	$('.toast').toast({
		'autohide': true,
		'delay': 3500,
		'animation': true
	});

	// Show the toast
  	$('.toast').toast('show');
  	toastContainer.classList.add("d-block");
  	toastContainer.classList.remove("d-none");
  	  	
  	// Actions after toast is destroyed
  	$('.toast').on('hidden.bs.toast', function () {
  		toastBody.innerHTML = "";
  		//toastContainer.style.display = 'none';
  		toastContainer.classList.add("d-none");
  		toastContainer.classList.remove("d-block");
	});
}

/**
 *  Add Remove Required Placeholder
 */
function addRemoveRequiredPlaceholder(action, arrayOfElements) {
	try {
		if (action == "add" || action == "remove") {
			arrayOfElements.forEach((element)=>{
				if (action == "add") {
					if (!element.placeholder.startsWith("* ")) {
						element.setAttribute("placeholder", "* " + element.placeholder.trim());
					}
				} else if (action == "remove") {
					if (element.placeholder.startsWith("* ")) {
						element.setAttribute("placeholder", element.placeholder.slice(1).trim());
					}
				}
			});
		}
	} catch(err) {
		console.error(err);
	}
}

/**
 * Show Block Element
 */
function showBlockElement(element) {
	try {
		element.classList.add("d-block");
		element.classList.remove("d-none");
	} catch(err) {
		console.error(err);
	}
}

/**
 * Hide Block Element
 */
function hideBlockElement(element) {
	try {
		element.classList.add("d-none");
		element.classList.remove("d-block");
	} catch(err) {
		console.error(err);
	}
}



	
	
	
	