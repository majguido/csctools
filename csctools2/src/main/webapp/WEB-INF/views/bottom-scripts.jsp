<%@ include file="/WEB-INF/views/tiles/layouts/include.jsp"%>
<script>
	var contextPath = "<%=request.getContextPath()%>";
	var homeLink = (contextPath == "") ? "/" : contextPath;
</script>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- JQUERY -->
<%-- <script src="<c:url value='/resources/vendor/jquery/jquery-3.6.0.min.js'/>"></script> --%>

<%-- <script src="<c:url value='/resources/vendor/other/popper.min.js'/>"></script> --%>
<!-- <script src="https://unpkg.com/@popperjs/core@2"></script> -->

<%-- <script src="<c:url value='/resources/vendor/bootstrap/js/bootstrap.min.js'/>"></script> --%>
 

<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
<%-- <script src="<c:url value='/resources/assets/js/vendor/holder.min.js'/>"></script> --%>


<!-- JQuery Separated -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>

<!-- JQuery Mask Script -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js"></script>

<!-- JQuery Mask Script Again?-->
<script src="<c:url value='/resources/vendor/jquery/jquery.mask.min.js'/>"></script>