<%@ include file="/WEB-INF/views/tiles/layouts/include.jsp" %>
<%@ include file="/WEB-INF/views/tiles/templates/defaultHeader.jsp" %>
<div class="mx-auto mt-5 w-100 center">
	<a class="" href="https://www.missouristate.edu/bearpass/">
		<img alt="Bear Pass" width="95" height="63" class="mb-2"
			 src="//missouristate.info/images/2015/bearpass/logo-sitetitle.png" 
			 srcset="//missouristate.info/images/2015/bearpass/logo-sitetitle.png 1.5x, //missouristate.info/images/2015/bearpass/logo-sitetitle-1x.png 1x" />
	</a>
	<hr style="width: 100%" />
	<div id="login-form-div" class="center">
		<form action="<%=request.getContextPath() %>/login" method="post" id="login-form" class="form">
			<div class="form-group">
				<input id="username" name="username" class="w-100" type="text" placeholder="Login" />	
			</div>
			<div class="form-group">
				<input id="password" name="password" class="w-100" type="password" placeholder="Password" />	
			</div>
			<div class="ContentBlock FormButton">
				<button class="w-100" name="submit" type="submit" value="Log in" tabindex="3">
					Log in
				</button>
			</div>
			<article class="ContentBlock HelpLinks">
				<p>
					<a href="https://missouristate.edu/Account">Forgot password?</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="https://missouristate.teamdynamix.com/TDClient/1931/Portal/Requests/ServiceDet?ID=38634">Need help?</a>
				</p>
				<p>
					<a href="https://mis.missouristate.edu/University/Account/Activate">Activate Your Account</a>
				</p>
				<p>
					<a href="https://www.missouristate.edu/disclaimer/">Policies and terms</a>
				</p>
			</article>
		</form>
	</div>
</div>
<script>
	document.addEventListener("DOMContentLoaded", ()=>{
		document.getElementById("loginlink").classList.add("d-none");
	});
</script>
<%@ include file="/WEB-INF/views/tiles/templates/defaultFooterSmall.jsp" %>


