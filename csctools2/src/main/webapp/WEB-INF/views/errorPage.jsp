<%@ include file="/WEB-INF/views/tiles/layouts/include.jsp"%>
<div class="container">
	<div class="row mt-3">
<%-- 		<div class="col-12 col-sm-3 text-center">
			<image class="img-fluid pb-2" id="errorImage" src="<%=request.getContextPath() %>/resources/img/errors/???.png" />
		</div> --%>
		<div class="col-12 col-sm-9">
			<div class="row">
				<div class="col-12 p-2">
					<h1>Oh No!</h1>
					<h3 class="text-justify pr-3">${message}</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-sm-2 p-1 mr-1">
					<%
						String contextPath = request.getContextPath();
						String homeUrl = ("".equals(contextPath)) ? "/" : contextPath; 
					%>
					<a href="<%=homeUrl %>" class="btn btn-primary w-100 mb-2">Home</a>
				</div>
				<div class="col-12 col-sm-2 p-1 mr-1">
					<a href="<%=request.getContextPath() %>/login" class="btn btn-primary w-100">Login</a>
				</div>
			</div>
		</div>
	</div>
</div>
<style>
</style>