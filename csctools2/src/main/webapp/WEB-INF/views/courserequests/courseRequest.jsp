<%@ include file="/WEB-INF/views/tiles/layouts/include.jsp"%>

<h2 class="mt-0">Course Requests</h2>
<p>
	If you would like to obtain permission to take a course in the future,
	please complete the form below and submit. You will be notified about the
	approval soon. 
</p>
<form:form action="${pageContext.request.contextPath}/students/courserequests" method="post" modelAttribute="courserequest">
 	<div class="row">
		<div class="col-12">
			<div class="form-group">
				<label for="course">Course</label>
			  	<form:select path="courseRequestId" class="form-control shadow-none">
			  		<form:option value="0">Select a Course</form:option>
					<c:forEach var="entry" items="${courseMap}">
						<form:option value="${entry.key}">${entry.value}</form:option>
					</c:forEach>
			  	</form:select>
			  	<form:errors path="courseRequestId" cssClass="formError" element="div" />
			</div>
		</div>
  		<div class="col-12 col-md-4">
			<div class="form-group">
				<label for="firstName">First Name</label>
				<form:input class="form-control shadow-none" path="firstName" placeholder="${courserequest.firstName}" readonly="true" />
			</div>
		</div>
		<div class="col-12 col-md-4">
			<div class="form-group">
			  <label for="lastName">Last Name</label>
			  <form:input class="form-control shadow-none" path="lastName" placeholder="Last Name" readonly="true" />
			</div>
		</div>
		<div class="col-12 col-md-4">
			<div class="form-group">
			  <label for="mNumber">Bear Number</label>
			  <form:input class="form-control shadow-none" path="mNumber" placeholder="M Number" />
			</div>
		</div>
		<div class="col-12 col-md-4">
			<div class="form-group">
			  <label for="email">Email</label>
			  <form:input class="form-control shadow-none" path="email" placeholder="Email" />
			</div>
		</div>
		<div class="col-12 col-md-4">
			<div class="form-group">
				<label for="semester">Semester</label>
				<form:select path="semester" class="form-control shadow-none">
					<form:option value="0">Select a Semester</form:option>
					<form:option value="Spring">Spring</form:option>
					<form:option value="Summer Intersession">Summer Intersession</form:option>
					<form:option value="Fall">Fall</form:option>
					<form:option value="Winter Intersession">Winter Intersession</form:option>
				</form:select>
			</div>
		</div>		
		<div class="col-12 col-md-4">
			<div class="form-group">
			  <label for="year">Year</label>
			  <select path="year" class="form-control shadow-none">
					<option value="0">Select a Year</option>
					<% 
						Integer thisYear = java.time.Year.now().getValue();
					  	for (int year=thisYear; year<(thisYear+3); year++) {
					  		out.write("<option path='year' value='"+year+"'>"+year+"</option>\n");
					  	}
					%>
				</select>
			</div>
		</div>
		<div class="col-12">
			<div class="form-group">
			  <label for="studentNotes">Student Notes</label>
			  <form:textarea class="form-control shadow-none" path="studentNotes" maxlength="4000" 
			  		    placeholder="Student Notes" value="${courseRequest.studentNotes}"></form:textarea>
			</div>
		</div>
		
	</div> <!-- End class="row" -->
	<div class="row">
		<div class="col-12">
			<button type="submit">Submit</button>
		</div>
	</div> 
</form:form>

