<%@ include file="/WEB-INF/views/tiles/layouts/include.jsp"%>

<h2 class="mt-0">Computer Science Tools</h2>
<p>
	The computer science department is proud to introduce a new web application 
	for Computer Science students (&quot;CSC Tools&quot;). The application will 
	hold tools that will automate processes and provide better communication and 
	flow for your computer science needs.
</p>
<p>
	The first tool we are introducing is the Course Request tool. If you would like
	to take a course, you will no longer email the Computer Science department. You
	can simply enter your requests here so that they can be tracked and processed
	quickly and easily. Please do let us know if you run into any bugs while working
	with the new tools. 
</p>
