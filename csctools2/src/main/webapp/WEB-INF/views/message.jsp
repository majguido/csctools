<%@ include file="/WEB-INF/views/tiles/layouts/include.jsp" %>
<%
// ********************************
// *
// *        MODEL MESSAGE
// *
// * ******************************
%>
<c:if test="${not empty messageBody}">
	<c:choose>
		<c:when test="${not empty messageType}">
			<c:choose>
				<c:when test="${messageType eq 'error'}">
					<div>
						<i class='fa fa-exclamation-circle text-danger strong' aria-hidden='true'></i>
						<span class='ml-2 text-danger strong'>${messageBody}</span>	
					</div>
				</c:when>
				<c:when test="${messageType eq 'success'}">
					<div>
						<i class='fa fa-check-circle text-success strong' aria-hidden='true'></i>
						<span class='ml-2 text-success strong'>${messageBody}</span>
					</div>	
				</c:when>
				<c:otherwise>
					<div>
						<i class='fa fa-info-circle text-info strong' aria-hidden='true'></i>
						<span class='ml-2 text-info strong'>${messageBody}</span>
					</div>	
				</c:otherwise>
			</c:choose>
		</c:when>
		<c:otherwise>
			<i class='fa fa-info-circle text-info strong' aria-hidden='true'></i>
			<span class='ml-2 text-info strong'>${messageBody}</span>
		</c:otherwise>
	</c:choose>
</c:if>
<%
	// ********************************
	// *
	// *        SESSION MESSAGE
	// *
	// * ******************************
%>
<c:set var="sessionMessageBody" value='<%=(String)session.getAttribute("messageBody")%>' />

<c:if test="${empty messageBody}">
	<%
		try {
			String messageBody = (String)session.getAttribute("messageBody");
			String messageType = (String)session.getAttribute("messageType");
			String messageColor = "";
			
			if (messageBody != null && messageBody.length() > 0 && messageType != null && messageType.length() > 0) {
				messageType = messageType.toLowerCase();
				
				switch(messageType) {
					case "error": messageColor = "text-danger"; break;
					case "info": messageColor = "text-info"; break;
					case "success": messageColor = "text-success"; break;
					case "warn": messageColor = "text-warning"; break;
					default: messageColor = "text-info"; break;
				}
				
				StringBuilder sb = new StringBuilder();
				sb.append("<div>\n");
				sb.append("\t<i class='fa fa-exclamation-circle text-danger strong' aria-hidden='true'></i>\n");
				sb.append("\t<span class='ml-2 "+messageColor+" strong'>"+messageBody+"</span>\n");
				sb.append("</div>\n");
				out.println(sb.toString());
			}
		} finally {
			session.removeAttribute("message");
		}
	%>
</c:if>
</script>