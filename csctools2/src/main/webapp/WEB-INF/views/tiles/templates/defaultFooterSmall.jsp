<%@ include file="/WEB-INF/views/tiles/layouts/include.jsp"%>
<footer id="footer-wrapper" class="mt-auto">
	<div id="footer" class="footer mt-5 p-3 text-center">
		<div id="footer-statement-div" class="row mb-3 mt-1">
			<div id="footer-statement" class="col-12" style="margin-top:-18px;">
				<div class="branding MYMS">
					<a href="https://www.MissouriState.edu/About/">
						<img class="DisplayOnly" src="<%=request.getContextPath() %>/resources/img/logos/myms-straight-reversed.svg" 
						     alt="Make Your Missouri Statement" />
					</a>
				</div>
			</div>
		</div>
		<div id="footer-links-div" class="row mb-1">
			<div class="col-12">
				<div class="legal">
			      <ul>
			         <li id="directedit-modified">Last Modified: September 12, 2022</li>
			      </ul>
			      <ul>
			         <li><a href="https://www.MissouriState.edu/Accessibility/">Accessibility</a></li>
			         <li><a href="https://www.MissouriState.edu/Disclaimer/">Disclaimer</a></li>
			         <li><a href="https://www.MissouriState.edu/Services/FedManInfo.htm">Disclosures</a></li>
			         <li><a href="https://www.MissouriState.edu/Equity/Nondiscrimination_Statement.htm">EO/AA/F/Veterans/Disability/Sexual Orientation/Gender Identity</a></li>
			      </ul>
			      <ul>
			         <li><span id="directedit"><a id="de" rel="nofollow" href="https://a.cms.omniupdate.com/11/?skin=oucampus&amp;account=missouristate&amp;site=ComputerScience&amp;action=de&amp;path=/default.pcf">�</a></span> 2019 <a href="https://www.MissouriState.edu/BOG/" aria-label="Board of Governors">Board of Governors, Missouri State University</a></li>
			         <li><a href="https://www.MissouriState.edu/Contact/" aria-label="Contact Missouri State">Contact Information</a></li>
			      </ul>
				</div>
			</div>
		</div>
	</div>
</footer> <!-- End id="footer" -->

