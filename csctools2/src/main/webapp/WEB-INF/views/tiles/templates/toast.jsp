<%@ include file="/WEB-INF/views/tiles/layouts/include.jsp" %>
<div id="toastContainer" class="d-none" role="alert" aria-live="assertive" aria-atomic="true">
	<div id="toastHeader" style="margin-bottom:0 !important" class="toast-header toast">
		<!-- <img id="toastImage" src="" class="rounded mr-2" alt="toast"> -->
		<i id="toastImage" class="fa-solid fa-bell text-white"></i>
		<strong id="toastHeading" class="mr-auto text-white"></strong>
		<!-- <small id="toastTiming" class="text-white">&nbsp;&nbsp;&nbsp;Just now</small> -->
		<button id="toastCloseButton" type="button" class="ml-2 mb-1 close text-white" data-dismiss="toast" aria-label="Close">
			<span class="text-white" aria-hidden="true">&times;</span>
		</button>
	</div>
	<div id="toastBody" class="toast-body">
	</div>
</div>