<%@ include file="/WEB-INF/views/tiles/layouts/include.jsp"%>
<header data-msutemplate="masthead" id="Masthead">
	<div id="MastheadInner">
		<div class="Logo">
			<div>
				<a href="https://www.MissouriState.edu/">
					<img src="<%=request.getContextPath() %>/resources/img/logos/sgf-logo.svg" 
	         			alt="Missouri State University" height="153" />
         		</a>
			</div>
		</div>
 		<div class="float-right pr-5">
			<security:authorize access="isAnonymous()">
				<a id="loginlink" href="<%=request.getContextPath() %>/login">Log in</a>
			</security:authorize>
			<security:authorize access="isAuthenticated()">
			    <a id="loginlink" href="<c:url value="/logout" />">Logout</a>
			</security:authorize>
		</div>
   </div>
</header>