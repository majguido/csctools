<%@ include file="/WEB-INF/views/tiles/layouts/include.jsp"%>
<nav aria-label="Breadcrumb" class="breadcrumb-new" role="navigation">
	<div id="Breadcrumb">
		<span class="ancestor">
			<a href="https://www.missouristate.edu/">
				<span>Missouri State</span>
			</a>
		</span>
		<span class="current-page">
			<span>CSC Tools</span>
		</span>
	</div>
</nav>