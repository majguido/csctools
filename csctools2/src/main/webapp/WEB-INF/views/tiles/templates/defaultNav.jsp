<%@ include file="/WEB-INF/views/tiles/layouts/include.jsp"%>
<%-- Load the context and homeLink path Java variables for future use --%>
<% 
 	String contextPath = request.getContextPath();
	String homeLink = ("".equals(contextPath)) ? "/" : contextPath;
%>

<!-- <nav aria-label="Main Menu" class="MainNav" id="MainNav">
	<div class="NavWrapper grid_inner">
		<div class="GlobalNav">
			<ul class="MainMenu" id="MainMenu">
				<li>
					<a href="https://www.MissouriState.edu/About/default.htm" target="_top" aria-label="Information About Missouri State">
						About MSU
					</a>
				</li>
				<li>
					<a href="https://www.missouristate.edu/FutureStudents/default.htm" target="_top" aria-label="Information About Missouri State">
						Future Students
					</a>
				</li>
				<li>
					<a href="https://www.missouristate.edu/CurrentStudents/default.htm" target="_top" aria-label="Information About Missouri State">
						Current Students
					</a>
				</li>
				<li>
					<a href="https://www.missouristate.edu/FacultyStaff/default.htm" target="_top" aria-label="Information About Missouri State">
						Faculty and Staff
					</a>
				</li>
				<li>
					<a href="https://alumni.missouristate.edu/default.htm" target="_top" aria-label="Information About Missouri State">
						Alumni
					</a>
				</li>
				<li>
					<a href="https://www.missouristate.edu/BearsFamilies/default.htm" target="_top" aria-label="Information About Missouri State">
						Family Programs
					</a>
				</li>
				<li>
					<a href="https://missouristatebears.com/" target="_top" aria-label="Information About Missouri State">
						Athletics
					</a>
				</li>
				
			</ul>	
		</div>
	</div>
</nav> -->

<!-- OLD NAV -->
<%-- <nav id="main-nav" class="navbar navbar-expand-md navbar-light bg-light shadow bg-white rounded">
	<a class="navbar-brand" href="<%=homeLink %>">
		<img id="csct-nav-fulllogo" class="img-fluid fadeIn" src="<c:url value="/resources/img/logos/csctools-42x32.png"/>" alt="CSC Tools Logo">
	</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item active">
				<a class="nav-link" href="<%=homeLink %>">Home <span class="sr-only">(current)</span></a>
			</li>

			<!-- WORKING -->
			<li class="nav-item">
				<a class="nav-link" href="<%=request.getContextPath() %>/registration">Register</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<%=request.getContextPath() %>/login">Login</a>
			</li>
		</ul>
	</div>
</nav> --%>
