<%@ include file="/WEB-INF/views/tiles/layouts/include.jsp"%>

<div id="site-nav" class="mb-4">
	<div id="site-inner-nav">
		<!-- LINKS FOR FACULTY AND STAFF -->
		<div class="headline">CSC Tools</div>
		<div class="link"><a href="<%=request.getContextPath() %>/students/courserequests">Course Requests</a></div>
		
		<!-- LINKS FOR FACULTY ONLY -->
		<security:authorize access="hasAnyRole('ADMIN', 'APPADMIN')">
			<hr/>
			<div class="mt-4">
				<div class="headline">App Management</div>
	    		<div class="link">
	    			<a href="<%=request.getContextPath()%>/admin/courserequestmgr">Course Request Manager</a>
	    		</div>
	    	</div>    	
		</security:authorize>
	</div>
</div>


<div id="dropdownDiv" class="foo">
	<button id="dropdownMenuBtn" class="dropdown-toggle" type="button"  
          data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		<span class="headline">CSC Tools</span>
  	</button>
  	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
  		<!-- LINKS FOR FACULTY AND STAFF -->
		<a class="dropdown-item" href="<%=request.getContextPath()%>/courserequest">Course Requests</a>
		
		<!-- LINKS FOR FACULTY ONLY -->
		<security:authorize access="hasAnyRole('ADMIN', 'APPADMIN')">
			<a class="dropdown-item" href="<%=request.getContextPath()%>/admin/courserequestmgr">Course Request Manager</a>
		</security:authorize> 
  	</div>
</div>

<style>
.dropdown-menu {
	width: 94%;
}
#dropdownMenuBtn {
	width: 100%;
	text-align: left;
	padding: 15px;
	margin-bottom: 20px;
	background-color: white;
	border-top: 12px solid #c2c2c2;
	border-top-color: #0093b2;
	border-left: 1px solid #ccc;
	border-right: 1px solid #ccc;
	border-bottom: 1px solid #ccc;
} 
.dropdown-toggle::after {
    vertical-align: middle;
    float: right;
    margin-top: 5px;
}
.dropdown-toggle .caret {
    position: absolute !important;
    Right: 16px !important;
    top: calc(50% - 2px) !important;
}
#site-nav {
    background-color: #fff;
    border-top: 12px solid #c2c2c2;
    border-color: #0093b2;
    padding: 10px;
}
#site-inner-nav {
	background-color: #f4f5f5;
	padding: 10px;
	font-weight: 600;
}

#site-inner-nav .headline {
    text-transform: uppercase;
    font-size: 1rem;
    line-height: 1;
    margin: 0 0 1rem 0;
    font-weight: 800 !important;
}

@media screen and (min-width: 1024px) {
	#site-nav {
		display: block;
	}	
	#dropdownDiv {
		display: none;
	}
}
@media screen and (max-width: 1023px) {
	#site-nav {
		display: none;
	}
	#dropdownDiv {
		display: block;
	}
}
</style>

