<%@ include file="/WEB-INF/views/tiles/layouts/include.jsp"%>
<!DOCTYPE HTML>
<html>
<%@ include file="../templates/head.jsp" %>
    <body class="d-flex flex-column min-vh-100">
	    <div class="flex-container">
	        <tiles:insertAttribute name="header" />
	        <div class="container">
		       	<div class="row">
		       		<div class="col-12">
		       			<tiles:insertAttribute name="body" />
		       		</div>
		       	</div>
	        </div>
	    	<tiles:insertAttribute name="footer" />
	    </div>
		<%@ include file="/WEB-INF/views/bottom-scripts.jsp"%>
		<%@ include file="/WEB-INF/views/tiles/templates/toast.jsp"%>
	</body>
</html>