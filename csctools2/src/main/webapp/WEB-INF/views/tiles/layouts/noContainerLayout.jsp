<%@ include file="/WEB-INF/views/tiles/layouts/include.jsp"%>
<!DOCTYPE HTML>
<html>
<%@ include file="../templates/head.jsp" %>
    <body>
    	<%-- <tiles:insertAttribute name="header" /> --%>
		<tiles:insertAttribute name="body" />
	    <%-- <tiles:insertAttribute name="footer" /> --%>
	    <%@ include file="/WEB-INF/views/bottom-scripts.jsp"%>
	    <%@ include file="/WEB-INF/views/tiles/templates/toast.jsp"%>
	</body>
</html>