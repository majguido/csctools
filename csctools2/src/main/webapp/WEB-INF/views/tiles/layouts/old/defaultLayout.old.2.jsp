<!DOCTYPE HTML>
<%-- <%@ include file="/WEB-INF/template/includes.jsp" %> --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html>
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<title><tiles:getAsString name="title" /></title>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="keywords" content="Bootstrap, Parallax, Template, Registration, Landing">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="description" content="<tiles:insertAttribute name="page_description" ignore="true"/>">
		
		<!-- Google Verification -->
		<meta name="google-site-verification" content="" />
				
		<!-- Fav Icon -->
		<link rel="icon" href="<c:url value="/resources/img/ico/favicon.ico"/>" type="image/x-icon" />
		<link rel="shortcut icon" href="<c:url value="/resources/img/ico/favicon.ico"/>" type="image/x-icon" />
		
<!-- 		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="<c:url value="/resources/vendor/css/bootstrap.min.css"/>" />
		<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> -->
		
		<link rel="stylesheet" href="<c:url value="/resources/vendor/css/font-awesome.min.css"/>" />
		<link rel="stylesheet" href="<c:url value="/resources/vendor/css/line-icons.css"/>" />
		<link rel="stylesheet" href="<c:url value="/resources/vendor/css/owl.carousel.css"/>" />
		<link rel="stylesheet" href="<c:url value="/resources/vendor/css/owl.theme.css"/>" />
		<link rel="stylesheet" href="<c:url value="/resources/vendor/css/nivo-lightbox.css"/>" />
		<link rel="stylesheet" href="<c:url value="/resources/vendor/css/magnific-popup.css"/>" />
		<link rel="stylesheet" href="<c:url value="/resources/vendor/css/slicknav.css"/>" />
		<link rel="stylesheet" href="<c:url value="/resources/vendor/css/animate.css"/>" />
		<link rel="stylesheet" href="<c:url value="/resources/vendor/css/main.css"/>" />
		<link rel="stylesheet" href="<c:url value="/resources/vendor/css/responsive.css"/>" />
		<link rel="stylesheet" href="<c:url value="/resources/css/styles.css"/>" />
		
		<!-- jQuery first, then Tether, then Bootstrap JS. -->
		<script src="<c:url value="/resources/vendor/js/jquery-min.js"/>"></script>
		<script src="<c:url value="/resources/vendor/js/popper.min.js"/>"></script>
		
		<script src="<c:url value="/resources/vendor/js/bootstrap.min.js"/>"></script>
		
		<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script> -->
		
		<script src="<c:url value="/resources/vendor/js/jquery.mixitup.js"/>"></script>
		<script src="<c:url value="/resources/vendor/js/nivo-lightbox.js"/>"></script>
		<script src="<c:url value="/resources/vendor/js/owl.carousel.js"/>"></script>
		<script src="<c:url value="/resources/vendor/js/jquery.stellar.min.js"/>"></script>
		<script src="<c:url value="/resources/vendor/js/jquery.nav.js"/>"></script>
		<script src="<c:url value="/resources/vendor/js/scrolling-nav.js"/>"></script>
		<script src="<c:url value="/resources/vendor/js/jquery.easing.min.js"/>"></script>
		<script src="<c:url value="/resources/vendor/js/smoothscroll.js"/>"></script>
		<script src="<c:url value="/resources/vendor/js/jquery.slicknav.js"/>"></script>
		<script src="<c:url value="/resources/vendor/js/wow.js"/>"></script>
		<script src="<c:url value="/resources/vendor/js/jquery.vide.js"/>"></script>
		<script src="<c:url value="/resources/vendor/js/jquery.counterup.min.js"/>"></script>
		<script src="<c:url value="/resources/vendor/js/jquery.magnific-popup.min.js"/>"></script>
		<script src="<c:url value="/resources/vendor/js/waypoints.min.js"/>"></script>
		<script src="<c:url value="/resources/vendor/js/form-validator.min.js"/>"></script>
		<script src="<c:url value="/resources/vendor/js/contact-form-script.js"/>"></script>
		<script src="<c:url value="/resources/vendor/js/main.js"/>"></script>
		
	</head>
    <body>
	    <div class="flex-container">
	        <tiles:insertAttribute name="header" />
	        <%-- <tiles:insertAttribute name="menu" /> --%>
	        <article class="article">
			    <tiles:insertAttribute name="body" />
	        </article>
	    	<tiles:insertAttribute name="footer" />
	    </div>
    
	</body>
</html>