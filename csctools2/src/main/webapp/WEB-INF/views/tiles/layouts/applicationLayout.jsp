<%@ include file="/WEB-INF/views/tiles/layouts/include.jsp"%>
<!DOCTYPE HTML>
<html>
<%@ include file="../templates/head.jsp" %>
    <body style="background-color: #f4f5f5;">
    	<tiles:insertAttribute name="header" />
    	<tiles:insertAttribute name="nav" />
	    <div id="titlecontainer" class="container-fluid" style="background-color: #fff;">
	    	<div class="container" style="background-color: #fff;">
				<%@ include file="/WEB-INF/views/tiles/templates/siteTitle.jsp"%>
				<%@ include file="/WEB-INF/views/tiles/templates/siteBreadcrumbs.jsp"%>
	    	</div>
	    </div>
		<div id="menu-body-container" class="container" style="background-color: #f4f5f5;">
			<div class="row mt-4">
				<div class="col-12 col-lg-4">
					<%@ include file="/WEB-INF/views/tiles/templates/siteNav.jsp"%>
				</div>
				<div class="col-12 col-lg-8">
					<tiles:insertAttribute name="body" />
				</div>
			</div>
		</div>
		<tiles:insertAttribute name="footer" />
	    <%@ include file="/WEB-INF/views/bottom-scripts.jsp"%>
	    <%@ include file="/WEB-INF/views/tiles/templates/toast.jsp"%>
	</body>
</html>