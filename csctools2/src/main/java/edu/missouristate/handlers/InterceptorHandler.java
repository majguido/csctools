package edu.missouristate.handlers;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import edu.missouristate.domain.User;
import edu.missouristate.service.UserService;
import edu.missouristate.util.helpers.SecurityHelper;

@Service
public class InterceptorHandler implements HandlerInterceptor {
		
	private int debug = 0;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		String uri = request.getRequestURI();
		StringBuffer url = request.getRequestURL();
		HttpSession session = request.getSession();
		
		if (debug > 0) {
			System.out.println("PRE-HANDLE! URI: " + uri + " url:" + url);
		}
		
		// Ensure a user object is in session if someone is visiting a student
		// or admin page
		if (uri != null && (uri.toLowerCase().contains("/students/") ||
				uri.toLowerCase().contains("/admin/"))) {
			
			// Get the user object from session
			User user = (User)session.getAttribute("user");
			
			// If the user object is null, check their authority
			if (user == null) {
				
		    	SecurityContext securityContext = SecurityContextHolder.getContext();
		    	boolean hasAuthority = SecurityHelper.hasAnyAuthority(securityContext, 
		    				Arrays.asList(new String[] {"ROLE_ADMIN","ROLE_STUDENT"}));
		    	
		    	if (hasAuthority) {
		    		session.setAttribute("uri", uri);
		    		response.sendRedirect("/dashboard");
		    		return false;
		    	}
		    	
				response.sendRedirect("/login");
				return false;
			}
		}
		
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		if (debug > 0) {
			System.out.println("POST-HANDLE (VIEW NOT CREATED YET)!");
		}
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception ex) {
		
		if (debug > 0) {
			System.out.println("AFTER-COMPLETION (VIEW CREATED)!");	
		}
	}
}