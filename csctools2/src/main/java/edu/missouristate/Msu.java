package edu.missouristate;

import java.math.BigDecimal;
import java.util.Map;

import edu.missouristate.domain.User;
import edu.missouristate.util.helpers.Helper;
import edu.missouristate.util.helpers.StringHelper;

public class Msu {

	public static final BigDecimal ZERO_DOLLARS_BD = new BigDecimal("0");

	public static final String GENERIC_ERROR = "Internal Error. Please refresh the page and try again. If this error persists, please contact the program office.";
	public static final String SUCCESS = "success";
	public static final String ERROR = "error";
	public static final String DANGER = "danger";
	public static final String INFO = "info";
	public static final String WARNING = "warning";
	public static final String MESSAGE = "message";
	public static final String MESSAGE_TYPE = "messageType";
	
	private static void sysout(String type, String message) {
		if (Msu.hasText(type) && Msu.hasText(message)) {
			String timestamp = StringHelper.pad(Helper.getMySQLCurrentTimestamp(), 23, " ");
			System.out.println(type + " " + timestamp + " " + message);
		}
	}
	
	public static void debug(String message) {
		sysout("[DEBUG]", message);
	}
	
	public static void info(String message) {
		sysout("[INFO ]", message);
	}

	public static void warn(String message) {
		sysout("[WARN ]", message);
	}
	
	public static void error(String message) {
		sysout("[ERROR]", message);
	}
	
	public static void printRuntime(String location, User user, long start, long stop) {
		
		if (user != null && ("admin".equalsIgnoreCase(user.getEmail())
				|| "jefferyadmin".equalsIgnoreCase(user.getEmail()))) {
			
			long time = (stop - start);
			String timeString = StringHelper.getMinutesAndSeconds(time);
			String message = location + " " + timeString;
			Msu.debug(message);
		}		
	}

	public static void sleep(long sleeptime) {
		
		try {
			Thread.sleep(sleeptime);
		} catch (Exception ex) {
			/* do nothing */
		}
	}

	public static boolean hasText(String str) {
		return Msu.hasText(str);
	}

	public static String getErrorMessage(Map<String, Object> responseMap) {
		String errorMessage = "";
		
		if (responseMap != null) {
			Object messageTypeObj = responseMap.get(Msu.MESSAGE_TYPE);
			
			if (messageTypeObj != null) {
				String messageType = messageTypeObj.toString();
				
				if (messageType.equalsIgnoreCase(Msu.DANGER) || messageType.equalsIgnoreCase(Msu.ERROR)) {
					Object messageObj = responseMap.get(Msu.MESSAGE_TYPE).toString();
					
					if (messageObj != null) {
						errorMessage = messageObj.toString();
					}
				}
				
			}
		}
		
		return errorMessage;
	}
}

