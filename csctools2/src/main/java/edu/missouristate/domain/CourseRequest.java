package edu.missouristate.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "courserequests")
public class CourseRequest {
	public CourseRequest() {

	}
	
	public CourseRequest(String firstName, String lastName) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "course_request_id") // course_request_id int NOT NULL AUTO_INCREMENT,
	private Integer courseRequestId;

	@Column(name = "status_id") // status_id int DEFAULT '4',
	private Integer statusId;

	@Column(name = "request_date") // request_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	private Timestamp requestDate;

	@Column(name = "course") // course VARCHAR(256) NOT NULL,
	private String course;

	@Column(name = "semester") // semester VARCHAR(64) NOT NULL,
	private String semester;
	
	@Column(name = "year") // year int NOT NULL,
	private String year;
	
	@Column(name = "first_name") // first_name VARCHAR(256) NOT NULL,
	private String firstName;

	@Column(name = "last_name") // last_name VARCHAR(256) NOT NULL,
	private String lastName;

	@Column(name = "email") // email VARCHAR(256) NOT NULL,
	private Integer email;

	@Column(name = "m_number") // m_number VARCHAR(32) NOT NULL,
	private Integer mNumber;

	@Column(name = "student_notes") // student_notes VARCHAR(4000),
	private String studentNotes;

	public Integer getCourseRequestId() {
		return courseRequestId;
	}

	public void setCourseRequestId(Integer courseRequestId) {
		this.courseRequestId = courseRequestId;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public Timestamp getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Timestamp requestDate) {
		this.requestDate = requestDate;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getEmail() {
		return email;
	}

	public void setEmail(Integer email) {
		this.email = email;
	}

	public Integer getmNumber() {
		return mNumber;
	}

	public void setmNumber(Integer mNumber) {
		this.mNumber = mNumber;
	}

	public String getStudentNotes() {
		return studentNotes;
	}

	public void setStudentNotes(String studentNotes) {
		this.studentNotes = studentNotes;
	}

	public String getSemester() {
		return semester;
	}

	public void setSemester(String semester) {
		this.semester = semester;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

}
