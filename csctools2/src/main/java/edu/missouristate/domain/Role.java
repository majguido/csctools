package edu.missouristate.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "roles")
public class Role {

	/**
	 * Program Office Roles
	 * poadmin
	 * poaccountant
	 * poprojectfacilitator
	 * 
	 * Fund Recipient Roles
	 * frfinance
	 * frfinancero
	 * fractivityro
	 * fractivity
	 * fradmin
	 * 
	 * State Roles
	 * stateadmin
	 */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "role_id")
    private int roleId;

    @Column(name = "role")
    private String role;
    
    @Column(name = "role_viewable")
    private String roleViewable;

    public Role() {
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

	public String getRoleViewable() {
		return roleViewable;
	}

	public void setRoleViewable(String roleViewable) {
		this.roleViewable = roleViewable;
	}
    
}
