package edu.missouristate.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "courses")
public class Course {
	public Course() {

	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "course_id") // course_id int NOT NULL AUTO_INCREMENT,
	private Integer courseId;

	@Column(name = "course_section") // course_section VARCHAR(12) NOT NULL,
	private String courseSection;

	@Column(name = "course_title") // course_title VARCHAR(256) NOT NULL,
	private String courseTitle;

	@Column(name = "course_crn") // course_crn int,
	private Integer courseCrn;

	@Column(name = "course_description") // course_description VARCHAR(2048),
	private String courseDescription;

	@Column(name = "status_id") // status_id int DEFAULT '1',
	private Integer statusId;

	public Integer getCourseId() {
		return courseId;
	}

	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}

	public String getCourseSection() {
		return courseSection;
	}

	public void setCourseSection(String courseSection) {
		this.courseSection = courseSection;
	}

	public String getCourseTitle() {
		return courseTitle;
	}

	public void setCourseTitle(String courseTitle) {
		this.courseTitle = courseTitle;
	}

	public Integer getCourseCrn() {
		return courseCrn;
	}

	public void setCourseCrn(Integer courseCrn) {
		this.courseCrn = courseCrn;
	}

	public String getCourseDescription() {
		return courseDescription;
	}

	public void setCourseDescription(String courseDescription) {
		this.courseDescription = courseDescription;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

}
