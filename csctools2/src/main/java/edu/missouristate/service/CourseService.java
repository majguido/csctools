package edu.missouristate.service;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import edu.missouristate.dao.CourseRepository;
import edu.missouristate.domain.CourseRequest;
import edu.missouristate.domain.User;
import edu.missouristate.util.enums.Status;

@Service
public class CourseService {

	@Autowired
	CourseRepository courseRepo;
	
	/**
	 * Get Course Map By Status
	 * @param statusId
	 * @return
	 */
	public Map<Integer, String> getCourseMapByStatus(Integer statusId) {
		Map<Integer, String> map = courseRepo.getCourseMapByStatus(statusId);
		return map;
	}

	/**
	 * Prepare Course Requests
	 * @param session
	 * @param model
	 * @param courseRequest 
	 */
	public void prepareCourseRequests(HttpSession session, Model model, CourseRequest courseRequest) {
		
		if (courseRequest == null) {
			User user = (User)session.getAttribute("user");
			courseRequest = new CourseRequest(user.getFirstName(), user.getLastName());
		}

		model.addAttribute("courserequest",courseRequest);
		Map<Integer, String> courseMap = getCourseMapByStatus(Status.ACTIVE.getIntValue());
		model.addAttribute("courseMap", courseMap);
	}

	/**
	 * Validate Course Request Form
	 * @param courseRequest
	 * @param result
	 */
	public void validateCourseRequestForm(CourseRequest courseRequest, BindingResult result) {
		
		Integer courseRequestId = courseRequest.getCourseRequestId();
		
//		if (courseRequestId == 0) {
//			result.addError(courseRequest.getCourseRequestId(), "Please select a course");
//		}
		
				
	}

	/**
	 * Save Course Request
	 * @param courseRequest
	 */
	@Transactional
	public void saveCourseRequest(CourseRequest courseRequest) {
		
	}
	
	
}
