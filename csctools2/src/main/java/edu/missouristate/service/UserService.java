package edu.missouristate.service;

import java.util.List;

import edu.missouristate.domain.User;
import edu.missouristate.model.UserDto;

public interface UserService {
    void saveUser(UserDto userDto);

    User findByEmail(String email);

    List<UserDto> findAllUsers();
}
