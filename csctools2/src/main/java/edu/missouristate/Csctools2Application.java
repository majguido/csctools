package edu.missouristate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Csctools2Application {

	public static void main(String[] args) {
		SpringApplication.run(Csctools2Application.class, args);
	}

}
