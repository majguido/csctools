package edu.missouristate.util.enums;

import java.util.HashMap;
import java.util.Map;

//import org.apache.commons.lang.WordUtils;
import org.springframework.util.StringUtils;

public enum Roles{
	
	ADMIN(1)
	,STUDENT(2)
	,APPADMIN(3)
	,CR_DIRECTOR(4)
	,CR_MANAGER(5)
	,CR_APPROVER(6)
	;
	
	private final int role;  

    Roles(int aRole) {  
        this.role = aRole;  
    }  
    
    public int getIntValue() {  
        return this.role;  
    }  
    
    public static String getRoleLiteral(Roles role) {
    	return role.toString();
    }
    
    public static String getViewableRoleLiteral(int inRole) {
    	
    	String roleLiteral = getRoleLiteral(inRole);
    	
    	if (StringUtils.hasText(roleLiteral)) {
    		roleLiteral = roleLiteral.replace("_", " ");
//    		roleLiteral = WordUtils.capitalizeFully(roleLiteral);
    	}
    	
    	return roleLiteral;
    }    
    
    public static String getRoleLiteral(int inRole) {
    	for(Roles currentRole: Roles.values()) {
    		if (currentRole.getIntValue() == inRole) {
    			return Roles.getRoleLiteral(currentRole);
    		}
    	}
    	return null;
    }
    
    private static final Map<Integer, Roles> intToTypeMap = new HashMap<Integer, Roles>();
    static {
        for (Roles type : Roles.values()) {
            intToTypeMap.put(type.role, type);
        }
    }

    public static Roles fromInt(int i) {
    	return intToTypeMap.get(Integer.valueOf(i));
    }

    public static Roles getRole(int inRole) {
    	for(Roles currentRole: Roles.values())
    		if (currentRole.getIntValue() == inRole)
    			return currentRole;
    	return null;
    }

    public static Roles getRole(String inRole) {
    	for(Roles currentRole: Roles.values())
    		if (Roles.getRoleLiteral(currentRole).equalsIgnoreCase(inRole) )
    			return currentRole;
    	return null;
    }
    
	public static Map<Integer, String> getRoleList() {
		Map<Integer, String> roleList = new HashMap<Integer, String>(); 
		
		for (Roles role : Roles.values())
			roleList.put(role.getIntValue(), role.toString());
		
		return roleList;
	}

} 