package edu.missouristate.util.enums;

import java.util.HashMap;
import java.util.Map;

//import com.gbt.util.Helpers.StringHelper;

public enum Type {

	EXISTING(1)
	,NEW(2)
	,OLD(3)
	;
	
    private final int type;
    
    Type(int type) {  
        this.type = type;  
    } 
    
    public int getIntValue() {  
        return this.type;  
    }  
    
    public static String getTypeLiteral(Type type) {
    	return type.toString();
    }
    
    public static String getViewableTypeLiteral(int inType) {
		String typeStr = Type.getTypeLiteral(inType);
//		typeStr = StringHelper.getViewableTypeString(typeStr);
		return typeStr;
    }
    
    public static String getTypeLiteral(int inType) {
    	for(Type currentType: Type.values()) {
    		if (currentType.getIntValue() == inType) {
    			return Type.getTypeLiteral(currentType);
    		}
    	}
    	return null;
    }
	
    private static final Map<Integer, Type> intToTypeMap = new HashMap<Integer, Type>();
    static {
        for (Type type : Type.values()) {
            intToTypeMap.put(type.type, type);
        }
    }

    public static Type fromInt(int i) {
    	return intToTypeMap.get(Integer.valueOf(i));
    }

    public static Type getType(int inType) {
    	for(Type currentType: Type.values())
    		if (currentType.getIntValue() == inType)
    			return currentType;
    	return null;
    }

    public static Type getType(String inType) {
    	for(Type currentType: Type.values())
    		if (Type.getTypeLiteral(currentType).equalsIgnoreCase(inType) )
    			return currentType;
    	return null;
    }
    
	public static Map<Integer, String> getTypeList() {
		Map<Integer, String> typeList = new HashMap<Integer, String>(); 
		
		for (Type type : Type.values())
			typeList.put(type.getIntValue(), type.toString());
		
		return typeList;
	}
	
}
