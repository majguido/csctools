package edu.missouristate.util.enums;

import java.util.HashMap;
import java.util.Map;

//import org.apache.commons.lang.WordUtils;
import org.springframework.util.StringUtils;

public enum Status{  
	ACTIVE(1)
	,INACTIVE(2)
	,DELETED(3)
	,PENDING(4)
	;
	
    private final int status;  
      
    Status(int aStatus) {  
        this.status = aStatus;  
    }  

    public int getIntValue() {  
        return this.status;  
    }  
    
    public String getIntValueAsString() {
    	Integer integer = this.status;
        return integer.toString();  
    }  
    
    public static String getStatusLiteral(Status status) {
    	return status.toString();
    }
    
    public static String getStatusLiteral(int inStatus) {
    	
    	for(Status currentStatus: Status.values())
    		if (currentStatus.getIntValue() == inStatus)
    			return Status.getStatusLiteral(currentStatus);
    	
    	return null;
    }
    
    public static String getViewableStatusLiteral(int inStatus) {
    	
    	if (inStatus == 0)
    		return "";
    	
		String status = Status.getStatusLiteral(inStatus);
		status = status.replace("_"," ");
//		status = WordUtils.capitalizeFully(status);
		return status;
    }
    
    public static Status getStatus(int inStatus) {
    	for(Status currentStatus: Status.values())
    		if (currentStatus.getIntValue() == inStatus)
    			return currentStatus;
    	return null;
    }

    public static Status getStatus(String inStatus) {
    	for(Status currentStatus: Status.values())
    		if (Status.getStatusLiteral(currentStatus).equalsIgnoreCase(inStatus) )
    			return currentStatus;
    	return null;
    }
    
    private static final Map<Integer, Status> intToTypeMap = new HashMap<Integer, Status>();
    static {
        for (Status type : Status.values()) {
            intToTypeMap.put(type.status, type);
        }
    }

    public static Status fromInt(int i) {
    	return intToTypeMap.get(Integer.valueOf(i));
    }

	public static Integer getStatusInt(String stringStatus) {
		Status status = null; 
				
		if (!StringUtils.hasText(stringStatus))
			return null;
			
		try {
			status = Status.valueOf(stringStatus.toUpperCase());	
		} catch (IllegalArgumentException iae) {
			return null;
		}
		
		if (status == null)
			return null;
		
		return status.getIntValue();
	}
 
	public static Map<Integer, String> getStatusList() {
		Map<Integer, String> statusList = new HashMap<Integer, String>(); 
		
		for (Status status : Status.values())
			statusList.put(status.getIntValue(), status.toString());
		
		return statusList;
	}
	
}