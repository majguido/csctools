package edu.missouristate.util.helpers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.net.NetworkInterface;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.Collator;
import java.text.DateFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.w3c.dom.Document;

import edu.missouristate.Msu;
import edu.missouristate.util.enums.Status;

@Component
public class Helper implements Serializable {
	
	public static final String BUFFERED_READER = "BufferedReader";
	public static final String FILE_READER = "FileReader";
	
	private static final long serialVersionUID = -1725757651678847625L;
	private static final Pattern UNDERSCORE_PATTERN_1 = Pattern.compile("([A-Z]+)([A-Z][a-z])");
	private static final Pattern UNDERSCORE_PATTERN_2 = Pattern.compile("([a-z\\d])([A-Z])");
		
	/**
	 * Add Session Message
	 * @param session
	 * @param messageType
	 * @param message
	 */
	public static void addSessionMessage(HttpSession session, String messageType, String message) {
		session.setAttribute(Msu.MESSAGE, message);
		session.setAttribute(Msu.MESSAGE_TYPE, messageType);
	}
	
	/**
	 * Add Model Attribute Message
	 * @param model
	 * @param messageType
	 * @param message
	 */
	public static void addModelAttributeMessage(Model model, String messageType, String message) {
		model.addAttribute(Msu.MESSAGE, message);
		model.addAttribute(Msu.MESSAGE_TYPE, messageType);
	}
	
	/**
	 * Add Redirect Attribute Message
	 * @param ra
	 * @param messageType
	 * @param message
	 */
	public static void addRedirectAttributeMessage(RedirectAttributes ra, String messageType, String message) {
		ra.addFlashAttribute(Msu.MESSAGE, message);
		ra.addFlashAttribute(Msu.MESSAGE_TYPE, messageType);
	}
	
	/**
	 * Has Text
	 * @param str
	 * @return
	 */
	public static boolean hasText(String str) {
		return StringUtils.hasText(str);
	}
		
	/**
	 * Write to Console
	 * @param clazz
	 * @param message
	 */
	public static void writeToConsole(String clazz, String message) {
		Msu.info("[" + clazz + "] " + message);
	}

	/**
	 * Write Service Message to Console
	 * @param clazz
	 * @param message
	 */
	public static void writeServiceMsgToConsole(String clazz, String message) {
		Msu.info(message + "\t : [" + clazz + "] ");
	}
	
	public static String javaCamelCaseToDotFormat(String camelCasedWord) {
		String dotWord = UNDERSCORE_PATTERN_1.matcher(camelCasedWord).replaceAll("$1_$2");
		dotWord = UNDERSCORE_PATTERN_2.matcher(dotWord).replaceAll("$1_$2");
		dotWord = dotWord.replace('-', '_').toLowerCase();
		dotWord = dotWord.replace('_', '.').toLowerCase();
		return dotWord;
	}
	
	// TODO: This is a horrible name for a function. Needs an intuitive name. 
	public static String underscore(String camelCasedWord) {
		
		if (camelCasedWord == null)
			return null;
		
		String underscoredWord = UNDERSCORE_PATTERN_1.matcher(camelCasedWord).replaceAll("$1_$2");
		underscoredWord = UNDERSCORE_PATTERN_2.matcher(underscoredWord).replaceAll("$1_$2");
		underscoredWord = underscoredWord.replace('-', '_').toLowerCase();
		return underscoredWord;
	}	

	// TODO: This is a horrible name for a function. Needs an intuitive name. 
	public static String propertyNameCase(String s) {
	     // Handle null and empty strings.
	     if (s == null || s.trim().length() == 0 ) return s;
	     String str;
	     if (s.indexOf('_') != -1) {

	         StringBuilder buf = new StringBuilder();
	         String[] tokens = s.split("_");
	         for (String token: tokens) {
	             if (token == null || token.length() == 0) continue;
	             buf.append(token.substring(0, 1).toUpperCase()).append(token.substring(1));
	         }
	         str = buf.toString();
	     } else {
	         str = s;
	     }
	     if (str.length()>  0) {
	         str = str.substring(0, 1).toLowerCase() + str.substring(1);
	     }
	     return str;
	} 
	
	public static String getEinSubstring(String str) {
		if (str == null)
			return null;
	
		try{
			String sub = str.substring(str.length() - 4, str.length());
			return sub;
		} catch (IndexOutOfBoundsException e) {
			return "";
		}
	} 

	public static Timestamp getMySQLCurrentTimestamp() {
		long timeNow = Calendar.getInstance().getTimeInMillis();
		return new java.sql.Timestamp(timeNow);
	}

	public static Timestamp getTime() {
		return getMySQLCurrentTimestamp();
	}
	
	public static String getMySQLCurrentTimestampNoMilliseconds() {
		long timeNow = Calendar.getInstance().getTimeInMillis();
		String timestamp = new java.sql.Timestamp(timeNow).toString();
		timestamp = timestamp.substring(0,(timestamp.length() - 4)); 
		return timestamp;
	}
	
	public static Timestamp getCurrentTimestamp() {
		Calendar calendar = Calendar.getInstance();
		Date now = calendar.getTime();
		return new Timestamp(now.getTime());
	}

	public static String getTodaysShortDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		return sdf.format(new java.util.Date());
	}

	public static String getTodaysMySqlShortDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(new java.util.Date());
	}
	
	public static Timestamp getTodaysShortTimestamp() {
		return Timestamp.valueOf(new SimpleDateFormat("yyyy-MM-dd").format(new Date()).concat(" 00:00:00"));
	}

	public static String getFilenameTimestamp() {
		String ts = new Timestamp(new java.util.Date().getTime()).toString();
		ts = ts.replace(":", "_").replace("-", "_").replace(" ", "_").replace(".", "_");
		return ts;
	}

	public static String getFilenameTimestamp(Timestamp timestamp) {
		String ts = timestamp.toString();
		ts = ts.replace(":", "_").replace("-", "_").replace(" ", "_").replace(".", "_");
		return ts;
	}

//	public static void main(String[] args) {
//		Ophi.info(getFilenameTimestamp());
//	}

	public static String getCurrentPage(HttpServletRequest request) {
		if (request != null) {
			String temp = request.getRequestURI();
			if (temp != null) {
				int index = temp.lastIndexOf('/');
				if (index > 0) {
					return temp.substring((index + 1), temp.length());
				}
			}
		}
		return "";
	}

	// This is legacy and probably should not be used....
	@Deprecated
	public static String stripHtmlTags(String htmlString) {
		if (htmlString != null)
			return htmlString.replaceAll("\\<.*?\\>", "").trim();
		return "";
	}

	public static boolean validateRequiredFormFields(HttpServletRequest request,
			String[] requiredFormElements) {
		for (String key : requiredFormElements) {
			if (isNullOrEmptyString(key, request))
				return false;
		}
		return true;
	}

	private static boolean isNullOrEmptyString(String key, HttpServletRequest request) {
		String value = request.getParameter(key);
		boolean isNull = (value == null);
		boolean isEmpty = ((value == null) ? true : value.isEmpty());
		return isNull || isEmpty;
	}

	public static String getMD5HashedString(String string) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(string.getBytes());
		byte byteData[] = md.digest();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}

	public static String getCurrentFolderWithPath(HttpServletRequest request) {
		if (request != null) {
			String temp = request.getRequestURI();
			if (temp != null) {
				int index = temp.lastIndexOf('/');
				if (index > 0) {
					return temp.substring(0, index);
				}
			}
		}
		return "";
	}

	public static String getCurrentResource(HttpServletRequest request) {
		if (request != null) {
			String context = request.getContextPath();
			String resourcePath = request.getRequestURI();
			return resourcePath.substring((resourcePath.indexOf(context) + context.length()), resourcePath.length());
		}
		return "";
	}

	public static String getWebApplicationRootContextPath(HttpServletRequest request) {
		if (request != null) {
			return request.getContextPath();
		}
		return "";
	}

	public static String getCleanContextPath(HttpServletRequest request) {
		String context = "";
		if (request != null) {
			context = request.getContextPath();
			if (context.equals("/")) {
				context = "";
			}
		}
		return context;
	}

	public static String DoctoString(Document doc) {
		try {
			StringWriter sw = new StringWriter();
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.transform(new DOMSource(doc), new StreamResult(sw));
			return sw.toString();
		} catch (Exception ex) {
			throw new RuntimeException("Error converting to String", ex);
		}
	}

	// Captcha was used at one point - we took it out - may be used again later ... 4/11/13 - JLB
//	public static Captcha getCaptcha() throws Exception {
//		Captcha captcha = new Captcha();
//		try {
//			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
//			DocumentBuilder db = dbf.newDocumentBuilder();
//			Document doc = db.parse(new URL(CAPTCHA_URL).openStream());
//			String xml = DoctoString(doc);
//			XStream xStream = new XStream(new DomDriver());
//			xStream.registerConverter(new CaptchaConverter());
//			xStream.alias(CAPTCHA, Captcha.class);
//			captcha = (Captcha) xStream.fromXML(xml);
//		} catch (Exception e) {
//			Random randomGenerator = new Random();
//			int num1 = (randomGenerator.nextInt(100) + 1);
//			int num2 = (randomGenerator.nextInt(100) + 5);
//			int sum = (num1 + num2);
//			captcha.setQuestion("What is the sum of " + num1 + " and " + num2);
//			captcha.addAnswer(getMD5HashedString(Integer.toString(sum)));
//		}
//		return captcha;
//	}

//	public static List<FileItem> getFileItemsFromMultiPartRequest(HttpServletRequest request)
//			throws Exception {
//
//		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
//		
//		if (isMultipart) {
//			DiskFileItemFactory factory = new DiskFileItemFactory();
//			ServletFileUpload upload = new ServletFileUpload(factory);
//			List<FileItem> fileItems = null;
//			fileItems = upload.parseRequest(request);
//			List<FileItem> fileItemsToReturn = new ArrayList<FileItem>();
//			Iterator<?> itr = fileItems.iterator();
//			
//			while (itr.hasNext()) {
//				FileItem item = (FileItem) itr.next();
//				String name = item.getName();
//				if (!item.isFormField()) {
//					if (name != null && !name.isEmpty()) {
//						fileItemsToReturn.add(item);
//					}
//				}
//			}
//			return fileItemsToReturn;
//		} else {
//			return null;
//		}
//	}

//	public static FormMultiPartRequest getFormMultiPartRequest(HttpServletRequest request)
//			throws Exception {
//
//		FormMultiPartRequest formMultiPartRequest = new FormMultiPartRequest();
//
//		if (ServletFileUpload.isMultipartContent(request)) {
//
//			Map<String,String> fieldValueMap = null;
//			List<FileItem> fileItemList = null;
//	        List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
//
//	        for (FileItem item : items) {
//
//	        	if (item.isFormField()) {
//	            	fieldValueMap = formMultiPartRequest.getFieldValueMap();
//	            	fieldValueMap.put(item.getFieldName(), item.getString());
//	            	formMultiPartRequest.setFieldValueMap(fieldValueMap);
//
//	        	} else {
//	            	fileItemList = formMultiPartRequest.getFileItemList();
//	            	fileItemList.add(item);
//	            	formMultiPartRequest.setFileItemList(fileItemList);
//	            }
//	        }
//		}
//		
//		return formMultiPartRequest;
//	}

	public static String getRootDocumentsLocation(HttpServletRequest request) {
		String rootContext = request.getServletContext().getRealPath("");
		int lastSlash = rootContext.lastIndexOf(System.getProperty("file.separator"));
		String subfolder = rootContext.substring(0, lastSlash);
		File file = new File(subfolder, "documents");
		String path = file.getAbsolutePath();
		path = path.replace("C:", "");
		path = path.replace("c:", "");
		path = path.replace("\\", "/");
		return path;
	}

	public static String getSessionStringByRequest(HttpServletRequest request, String name) {
		return (String) getSessionObjectBySession(request.getSession(), name);
	}

	public static String getSessionStringBySession(HttpSession session, String name) {
		return (String) getSessionObjectBySession(session, name);
	}

	public static Object getSessionObjectByRequest(HttpServletRequest request, String name) {
		return getSessionObjectBySession(request.getSession(), name);
	}

	public static Object getSessionObjectBySession(HttpSession session, String name) {
		return session.getAttribute(name);
	}

	public static void setSessionValueByRequest(HttpServletRequest request, boolean createNewSession,
			String name, Object value) {
		HttpSession session = null;
		if (createNewSession)
			session = request.getSession(true);
		else
			session = request.getSession();

		setSessionValue(session, name, value);
	}

	public static void setSessionValue(HttpSession session, String name, Object value) {
		session.setAttribute(name, value);
	}

	public static boolean isNumeric(String string) {
		if (string == null)
			return false;

		try {
			Integer.parseInt(string);
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}

	public static boolean containsOnlyNumbers(String string) {
		
		if (!string.matches("[0-9]+"))
			return false;
		
		return true;
	}
	
	public static boolean isNotInteger(String string) {
		
		if (string == null)
			return true;
		
		try {
			Integer.parseInt(string);
			return false;
		} catch (NumberFormatException nfe) {
			return true;
		}
	}
	
	public static boolean isInteger(String integer) {
		try {
			Integer.parseInt(integer);
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}
	
	public static boolean isInteger(Object[] objectsToCheck) {
		for (Object obj : objectsToCheck) {
			if (obj == null || isNotInteger(obj.toString()))
				return false;
		}
		return true;
	}
	
	public static boolean isInteger(char c) {
		try {
			String s = String.valueOf(c);
			Integer.parseInt(s);
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}
	
	public static boolean isNotNumeric(String string) {
		
		try {
			new BigDecimal(string);
			return false;
		} catch (Exception e) {
			return true;
		}
	}

//	public static File getDataFolder(HttpServletRequest request, ServletContext servletContext, 
//			OphiSettingsAPI gbtSettingsApi) {
//		
//		File dataDirectory = null;
//		Integer dataDirectoryFlag = null;
//		OphiSetting gbtSetting = null; 
//				
//		if (gbtSettingsApi != null) {
//			gbtSetting = gbtSettingsApi.getSettingByKey(OphiSettingsAPI.DATA_DIRECTORY);
//			dataDirectoryFlag = (gbtSetting.getIntValue() == null ? -1 : gbtSetting.getIntValue());	
//		}
//		
//		if (dataDirectoryFlag != null && dataDirectoryFlag == 1) {
//			dataDirectory = new File(gbtSetting.getStringValue());
//		} else {
//			if (request != null)
//				dataDirectory = new File(getTomcatFolder(request), "data");
//			else if (servletContext != null)
//				dataDirectory = new File(getTomcatFolder(servletContext), "data");
//		}
//		
//		if (dataDirectory != null && !dataDirectory.exists())
//			dataDirectory.mkdir();
//		
//		return dataDirectory;
//	}
	
//	public static File getSystemFolder(HttpServletRequest request, ServletContext servletContext, OphiSettingsAPI gbtSettingsApi) {
//		File systemDir = new File(getDataFolder(request, servletContext, gbtSettingsApi), "system");
//		
//		if (!systemDir.exists())
//			systemDir.mkdir();
//		
//		return systemDir;
//	}
//	
//	public static File getClientFolder(HttpServletRequest request, ServletContext servletContext, OphiSettingsAPI gbtSettingsApi) {
//
//		File clientDir = new File(getDataFolder(request, servletContext, gbtSettingsApi), "client");
//		
//		if (!clientDir.exists())
//			clientDir.mkdir();
//		
//		return clientDir;
//	}
//	
//	public static File getTempFolder(HttpServletRequest request, ServletContext servletContext, OphiSettingsAPI gbtSettingsApi) {
//		
//		File tempDir = new File(getDataFolder(request, servletContext, gbtSettingsApi), "temp");
//		
//		if (!tempDir.exists())
//			tempDir.mkdir();
//		
//		return tempDir;
//	}



//	public static File getShipmentsFolder(HttpServletRequest request, ServletContext servletContext, OphiSettingsAPI gbtSettingsApi) {
//
//		File shipmentsDir = new File(getClientFolder(request, servletContext, gbtSettingsApi), "shipments");
//		
//		if (!shipmentsDir.exists())
//			shipmentsDir.mkdir();
//		
//		return shipmentsDir;
//	}
	
//	public static File getCarrierFTPFolder(HttpServletRequest request, ServletContext servletContext, 
//			OphiSettingsAPI gbtSettingsApi, Integer identity) throws Exception {
//		
//		File carrierDir = getIdentityFolder(request, servletContext, identity, gbtSettingsApi);
//		
//		if (!carrierDir.exists())
//			throw new Exception("Carrier folder does not exist for MC #"+identity);
//			
//		File baseFolder = new File(carrierDir, "ftp");
//		
//		// Base FTP Folders are created special per carrier. The unix user "root" should own the folder and the 
//		// group should be "sftp_only_users." If the folder does not exist, we should not continue.
//		if (!baseFolder.exists()) {
//			throw new Exception("The base ftp folder has not been created for carrier with MC #"+identity);
//		} else {
//			File ftpFolder = new File(baseFolder, "ftp");
//			
//			if (!ftpFolder.exists()) {
//				throw new Exception("The ftp folder has not been created for carrier with MC #"+identity);
//			} else {
//				return ftpFolder;
//			}
//		}
//	}
	
//	public static File getCarrierFolder(HttpServletRequest request, ServletContext servletContext, OphiSettingsAPI gbtSettingsApi) {
//		
//		File carrierDir = new File(getClientFolder(request, servletContext, gbtSettingsApi), "carrier");
//		
//		if (!carrierDir.exists())
//			carrierDir.mkdir();
//		
//		return carrierDir;
//	}
//	
//	public static File getShipperFolder(HttpServletRequest request, ServletContext servletContext, OphiSettingsAPI gbtSettingsApi) {
//		
//		File shipperDir = new File(getClientFolder(request, servletContext, gbtSettingsApi), "shipper");
//		
//		if (!shipperDir.exists())
//			shipperDir.mkdir();
//		
//		return shipperDir;
//	}
//	
//	public static File getOtherFolder(HttpServletRequest request, ServletContext servletContext, OphiSettingsAPI gbtSettingsApi) {
//		
//		File otherDir = new File(getClientFolder(request, servletContext, gbtSettingsApi), "other");
//		
//		if (!otherDir.exists())
//			otherDir.mkdir();
//		
//		return otherDir;
//	}
	
//	public static File getIdentityFolder(HttpServletRequest request, ServletContext servletContext, Integer identity, OphiSettingsAPI gbtSettingsApi) {
//		
//		File userDir;
//		
//		if (identity < 3000000)
//			userDir = new File(getCarrierFolder(request, servletContext, gbtSettingsApi), String.valueOf(identity.intValue()));
//		else if (identity < 9000000)
//			userDir = new File(getOtherFolder(request, servletContext, gbtSettingsApi), String.valueOf(identity.intValue()));
//		else
//			userDir = new File(getShipperFolder(request, servletContext, gbtSettingsApi), String.valueOf(identity.intValue()));
//		
//		if (!userDir.exists())
//			userDir.mkdir();
//		
//		return userDir;
//	}
	
//	public static File getUserEsignFolder(HttpServletRequest request, Integer identity, OphiSettingsAPI gbtSettingsApi) {
//		
//		File userEsignDir = new File(getIdentityFolder(request, null, identity, gbtSettingsApi), "esign");
//		
//		if (!userEsignDir.exists())
//			userEsignDir.mkdir();
//		
//		return userEsignDir;
//	}

//	public static File getSystemDataFolder(HttpServletRequest request, ServletContext servletContext, 
//			OphiSettingsAPI gbtSettingsApi) {
//		
//		File systemDataDirectory = null;
//		File dataFolder = getDataFolder(request, servletContext, gbtSettingsApi);
//		
//		systemDataDirectory = new File(dataFolder, "system");
//		
//		if (systemDataDirectory != null && !systemDataDirectory.exists())
//			systemDataDirectory.mkdir();
//		
//		return systemDataDirectory;
//	}
	
	public static File getAgreementsFolder(HttpServletRequest request) {
		String rootContext = request.getServletContext().getRealPath("");
		File dir = new File(new File(rootContext, "assets"), "agreements");
		return dir;
	}

	public static File getTomcatLogsFolder(HttpServletRequest request, ServletContext servletContext) {
		File logsDir = null;
		File tomcatDir = getTomcatFolder(request, servletContext);
		
		if (tomcatDir != null) {
			logsDir = new File(tomcatDir, "logs");
			
			if (!logsDir.exists())
				logsDir.mkdir();
		}
		
		return logsDir;
	}
	
	public static String getRootContext(HttpServletRequest request, ServletContext servletContext) {
		String rootContext = null;
		
		if (request != null) {
			rootContext = request.getServletContext().getRealPath("");
		} else if (servletContext != null) {
			servletContext.getRealPath("");
		}
		
		return rootContext;
	}

	public static File getTomcatFolder(HttpServletRequest request, ServletContext servletContext) {
		if (request != null)
			return getTomcatFolder(request);
		else if (servletContext != null)
			return getTomcatFolder(servletContext);
		else 
			return null;
	}
	
	public static File getTomcatFolder(HttpServletRequest request) {
		String rootContext = request.getServletContext().getRealPath("");
		File dir = new File(rootContext).getParentFile().getParentFile();
		return dir;
	}
	
	public static File getTomcatFolder(ServletContext servletContext) {
		String rootContext = servletContext.getRealPath("");
		File dir = new File(rootContext).getParentFile().getParentFile();
		return dir;
	}

	public static File getTomcatApplicationFolder(HttpServletRequest request) {
		String rootContext = request.getServletContext().getRealPath("");
		return new File(rootContext);
	}	
	
	public static File getTomcatApplicationFolder(ServletContext servletContext) {
		String rootContext = servletContext.getRealPath("");
		return new File(rootContext);
	}	

	public static File getAssetsImgFolder(HttpServletRequest request) {
		File assetsFolder = getAssetsFolder(request);
		return new File(assetsFolder, "img");
	}

	public static File getDataProviderFolder(HttpServletRequest request, ServletContext servletContext) {
		File dataProviderDir = null;
		
		if (request == null && servletContext == null ) {
			String eclipseProjectFolder = getEclipseProjectFolder();
			String path = eclipseProjectFolder + "/src/test/com/gbt/dataprovider";
			dataProviderDir = new File(path);
			return dataProviderDir;
		} else {
			File assetsDir = getAssetsFolder(request, servletContext);
			
			if (assetsDir != null) {
				dataProviderDir = new File(assetsDir, "logs");
				
				if (!dataProviderDir.exists())
					dataProviderDir.mkdir();
			}
			
			return dataProviderDir;	
		}
	}
	
	private static String getEclipseProjectFolder() {
		File file = new File("");
		String path = file.getAbsolutePath().replace("C:", "").replace("\\", "/");
		return path;
	}

	public static File getAssetsFolder(HttpServletRequest request, ServletContext servletContext) {
		if (request != null)
			return getAssetsFolder(request);
		else if (servletContext != null)
			return getAssetsFolder(servletContext);
		else 
			return null;
	}
	
	public static File getAssetsFolder(HttpServletRequest request) {
		File appFolder = getTomcatApplicationFolder(request);
		return new File(appFolder, "assets");
	}
	
	public static File getAssetsFolder(ServletContext servletContext) {
		File appFolder = getTomcatApplicationFolder(servletContext);
		return new File(appFolder, "assets");
	}	

	public static File getWebinfJspMobileFolder(HttpServletRequest request) {
		File appFolder = getTomcatApplicationFolder(request);
		File webinf = new File(appFolder, "WEB-INF");
		File jsp = new File (webinf, "jsp");
		return new File(jsp, "mobile");
	}
	
	public static File getAgreementsFolder(ServletContext servletContext) {
		File assetsFolder = getAssetsFolder(servletContext);
		return new File(assetsFolder, "agreements");
	}	
	
	public static File getOphiDataFolder(HttpServletRequest request) {
		String rootContext = request.getServletContext().getRealPath("");
		String fileSeparator = System.getProperty("file.separator");

		for (int i = 0; i < 2; i++) {
			int index = rootContext.lastIndexOf(fileSeparator);
			rootContext = rootContext.substring(0, index);
		}

		String rootFolder = "gbtdata";
		File dir = new File(rootContext, rootFolder);
		return dir;
	}

	public static String hexEncode(byte[] aInput) {
		StringBuilder result = new StringBuilder();
		char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
		for (int idx = 0; idx < aInput.length; ++idx) {
			byte b = aInput[idx];
			result.append(digits[(b & 0xf0) >> 4]);
			result.append(digits[b & 0x0f]);
		}
		return result.toString();
	}

	public static Timestamp formatTimeStamp(Timestamp timestamp) {
		Timestamp ts = new Timestamp(1970, 0, 1, 0, 0, 0, 0);
		if (timestamp == null || timestamp.toString().equals("0000-00-00 00:00:00"))
			return ts;
		return timestamp;
	}

	// Pass any class into this method. It will reverse engineer the path and
	// find the WEB-INF folder
	public static File getParentFolder(Class<?> clazz, String folderName) throws Exception {
		URL url = clazz.getResource("");
		File parentFolder = null;
		boolean found = false;
		parentFolder = new File(url.toURI());

		try {
			for (int i = 0; i < 20; i++) {
				parentFolder = parentFolder.getParentFile();
				if (parentFolder.getPath().endsWith(folderName) == true) {
					found = true;
					break;
				}
			}
		} catch (NullPointerException e) {
			return null;
		}

		if (!found)
			return null;

		return parentFolder;
	}

	public static String getShortMySQLDateFourYear(Date date) {
		if (date == null)
			return null;
		final String DESIRED_FORMAT = "yyyy-MM-dd";
		SimpleDateFormat sdf = new SimpleDateFormat(DESIRED_FORMAT);
		return sdf.format(date);
	}

	public static String getShortSQLDateFourYear(Date date) {
		if (date == null)
			return null;
		
		final String DESIRED_FORMAT = "yyyy-MM-dd";
		SimpleDateFormat sdf = new SimpleDateFormat(DESIRED_FORMAT);
		return sdf.format(date);
	}
	
	public static String getShortJavaDateFourYear(Date date) {
		if (date == null)
			return null;
		final String DESIRED_FORMAT = "MM/dd/yyyy";
		SimpleDateFormat sdf = new SimpleDateFormat(DESIRED_FORMAT);
		return sdf.format(date);
	}

	public static String getShortJavaDateTwoYear(Date date) {
		if (date == null)
			return null;
		final String DESIRED_FORMAT = "MM/dd/yy";
		SimpleDateFormat sdf = new SimpleDateFormat(DESIRED_FORMAT);
		return sdf.format(date);
	}

	public static String getPresentableDateTimeBySqlTimestamp(Timestamp ts, String format, 
			boolean isHtml) throws ParseException {
		
		if (ts == null)
			return null;
		
		SimpleDateFormat formatter, FORMATTER;
		formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String oldDate = ts.toString();
		Date date = formatter.parse(oldDate.substring(0, oldDate.length()));
		FORMATTER = new SimpleDateFormat(format);
		String formatted = FORMATTER.format(date);
		
		if (isHtml)
			formatted = formatted.replace(" ", "&nbsp;&nbsp;");
		
		return formatted;
	}
	
	public static String getPresentableDateTimeBySqlTimestamp(Timestamp ts) throws ParseException {
		if (ts == null)
			return null;
		SimpleDateFormat formatter, FORMATTER;
		formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String oldDate = ts.toString();
		Date date = formatter.parse(oldDate.substring(0, oldDate.length()));
		FORMATTER = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		String formatted = FORMATTER.format(date);
		formatted = formatted.replace(" ", "&nbsp;&nbsp;");
		return formatted;
	}
	
	public static String getPresentableDateTimeBySqlTimestampAmPm(Timestamp ts) throws ParseException {
		if (ts == null)
			return null;
		SimpleDateFormat formatter, FORMATTER;
		formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String oldDate = ts.toString();
		Date date = formatter.parse(oldDate.substring(0, oldDate.length()));
		FORMATTER = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
		String formatted = FORMATTER.format(date);
		formatted = formatted.replace(" ", "&nbsp;&nbsp;");
		return formatted;
	}
	
	public static String getPresentableJavaDate(String oldDateString) throws ParseException {
		final String OLD_FORMAT = "yyyy-MM-dd";
		final String NEW_FORMAT = "MM/dd/yyyy";
		SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
		java.util.Date d = sdf.parse(oldDateString);
		sdf.applyPattern(NEW_FORMAT);
		return sdf.format(d);
	}
	
	public static java.util.Date getJavaDate(String oldDateString) {
		try {
			final String OLD_FORMAT = "yyyy-MM-dd";
			final String NEW_FORMAT = "MM/dd/yyyy";
			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			java.util.Date d = sdf.parse(oldDateString);
			sdf.applyPattern(NEW_FORMAT);
			String newDateString = sdf.format(d);
			java.util.Date newFormattedDate = sdf.parse(newDateString);
			return newFormattedDate;
		} catch (ParseException e) {
			return null;
		}
	}

	public static java.sql.Date getMySQLDate(Date oldDate) throws ParseException {
		
		if (oldDate == null)
			return null;
			
		final String OLD_FORMAT = "MM/dd/yyyy";
		final String NEW_FORMAT = "yyyy-MM-dd";
		SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
		String oldDateString = sdf.format(oldDate);
		java.util.Date d = sdf.parse(oldDateString);
		sdf.applyPattern(NEW_FORMAT);
		String newDateString = sdf.format(d);
		java.util.Date newFormattedDate = sdf.parse(newDateString);
		java.sql.Date newDate = new java.sql.Date(newFormattedDate.getTime());
		return newDate;	
	}

	public static String getStateAbbreviationByCityState(String cityState) {
		String abbr = null;
		Map<String, String> stateMap = new HashMap<String, String>();
		stateMap = initializeStateMap(stateMap);
        
		int index = cityState.indexOf(",");
		if (index > 0) {
			String state = cityState.substring(index+1, cityState.length());
			abbr = stateMap.get(state.trim().toUpperCase());
		}
		return abbr;
	}
	
	public static boolean isValidState(String state) {
		
		Map<String, String> stateMap = initializeStateMap();
		Map<String, String> provinceMap = initializeProvinceMap();
		
		if (!StringUtils.hasText(state))
			return false;	
		
		for (Map.Entry<String, String> entry : stateMap.entrySet())
		    if (state.equalsIgnoreCase(entry.getValue()))
		    	return true;
		
		for (Map.Entry<String, String> entry : provinceMap.entrySet())
		    if (state.equalsIgnoreCase(entry.getValue()))
		    	return true;
		
		return false;
	}
	
	public static Map<String, String> initializeStateMap() {
		return initializeStateMap(new HashMap<String, String>());
	}
	
	public static Map<String, String> initializeProvinceMap() {
		return initializeProvinceMap(new HashMap<String, String>());
	}
	
	public static Map<String, String> initializeStateMap(Map<String, String> stateMap) {
		stateMap.put("ALABAMA", "AL");
		//stateMap.put("ALASKA", "AK");
		stateMap.put("AMERICAN SAMOA", "AS");
		stateMap.put("ARIZONA", "AZ");
		stateMap.put("ARKANSAS", "AR");
		stateMap.put("CALIFORNIA", "CA");
		stateMap.put("COLORADO", "CO");
		stateMap.put("CONNECTICUT", "CT");
		stateMap.put("DELAWARE", "DE");
		stateMap.put("DISTRICT OF COLUMBIA", "DC");
		stateMap.put("FEDERATED STATES OF MICRONESIA", "FM");
		stateMap.put("FLORIDA", "FL");
		stateMap.put("GEORGIA", "GA");
		stateMap.put("GUAM GU", "GU");
		//stateMap.put("HAWAII", "HI");
		stateMap.put("IDAHO", "ID");
		stateMap.put("ILLINOIS", "IL");
		stateMap.put("INDIANA", "IN");
		stateMap.put("IOWA", "IA");
		stateMap.put("KANSAS", "KS");
		stateMap.put("KENTUCKY", "KY");
		stateMap.put("LOUISIANA", "LA");
		stateMap.put("MAINE", "ME");
		stateMap.put("MARSHALL ISLANDS", "MH");
		stateMap.put("MARYLAND", "MD");
		stateMap.put("MASSACHUSETTS", "MA");
		stateMap.put("MICHIGAN", "MI");
		stateMap.put("MINNESOTA", "MN");
		stateMap.put("MISSISSIPPI", "MS");
		stateMap.put("MISSOURI", "MO");
		stateMap.put("MONTANA", "MT");
		stateMap.put("NEBRASKA", "NE");
		stateMap.put("NEVADA", "NV");
		stateMap.put("NEW HAMPSHIRE", "NH");
		stateMap.put("NEW JERSEY", "NJ");
		stateMap.put("NEW MEXICO", "NM");
		stateMap.put("NEW YORK", "NY");
		stateMap.put("NORTH CAROLINA", "NC");
		stateMap.put("NORTH DAKOTA", "ND");
		stateMap.put("NORTHERN MARIANA ISLANDS", "MP");
		stateMap.put("OHIO", "OH");
		stateMap.put("OKLAHOMA", "OK");
		stateMap.put("OREGON", "OR");
		stateMap.put("PALAU", "PW");
		stateMap.put("PENNSYLVANIA", "PA");
		//stateMap.put("PUERTO RICO", "PR");
		stateMap.put("RHODE ISLAND", "RI");
		stateMap.put("SOUTH CAROLINA", "SC");
		stateMap.put("SOUTH DAKOTA", "SD");
		stateMap.put("TENNESSEE", "TN");
		stateMap.put("TEXAS", "TX");
		stateMap.put("UTAH", "UT");
		stateMap.put("VERMONT", "VT");
		stateMap.put("VIRGIN ISLANDS", "VI");
		stateMap.put("VIRGINIA", "VA");
		stateMap.put("WASHINGTON", "WA");
		stateMap.put("WEST VIRGINIA", "WV");
		stateMap.put("WISCONSIN", "WI");
		stateMap.put("WYOMING", "WY");
		stateMap.put("Armed Forces Africa", "AE");
		stateMap.put("Armed Forces Americas", "AA");
		stateMap.put("Armed Forces Canada", "AE");
		stateMap.put("Armed Forces Europe", "AE");
		stateMap.put("Armed Forces Middle East", "AE");
		stateMap.put("Armed Forces Pacific", "AP");
		
		stateMap.put("DISTRICT OF COLUMBIA", "DC");
		stateMap.put("WASHINGTON, D.C.", "DC");
		return stateMap;
	}
	
	public static Map<String, String> initializeProvinceMap(Map<String, String> provinceMap) {		
		provinceMap.put("ALBERTA", "AB");
		provinceMap.put("BRITISH COLUMBIA", "BC");
		provinceMap.put("MANITOBA", "MB");
		provinceMap.put("NEW BRUNSWICK", "NB");
		provinceMap.put("NEWFOUNDLAND AND LABRADOR", "NL");
		provinceMap.put("NOVA SCOTIA", "NS");
		provinceMap.put("NORTHWEST TERRITORIES", "NT");
		provinceMap.put("NUNAVUT", "NU");
		provinceMap.put("ONTARIO", "ON");
		provinceMap.put("PRINCE EDWARD ISLAND", "PE");
		provinceMap.put("QUEBEC", "QC");
		provinceMap.put("SASKATCHEWAN", "SK");
		provinceMap.put("YUKON", "YT");
		return provinceMap;
	}

//	public static Type getUserType(User user) throws Exception {
//		Type userType = null;
//		if (user.getMcNumber() != null)
//			userType = Type.CARRIER;
//		if (user.getShipperId() != null)
//			userType = Type.SHIPPER; 
//		if (userType != null)
//			return userType;
//		else
//			throw new Exception("Cannot find user type.");
//	}	
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Map<String, String> sortStringMapByValue(Map<String, String> map) {

		List<String> list = new LinkedList(map.entrySet());

		Collections.sort(list, new Comparator() {
			public int compare(Object o1, Object o2) {
				return ((Comparable) ((Map.Entry) (o1)).getValue()).compareTo(((Map.Entry) (o2)).getValue());
			}
		});

		Map<String, String> result = new LinkedHashMap<String, String>();

		for (Iterator it = list.iterator(); it.hasNext();) {
			//Map.Entry<String, String> entry = (Map.Entry<String, String>) it.next();
			Map.Entry entry = (Entry) it.next();
			result.put((String)entry.getKey(), (String)entry.getValue());
		}

		return result;
	}

	public static String prettyPrint(String header, String data, String delimiter, 
			Integer headerLength, boolean carriageReturn) {
		
		if (delimiter == null)
			delimiter = "";
		
		int delimiterLength = delimiter.length() + 2;
		
		if (headerLength == null) {
			if (carriageReturn)
				return header + " " + delimiter + " " + data + "\n";
			else
				return header + " " + delimiter + " " + data;
		}
		
		StringBuilder sb = new StringBuilder();
		sb.append(header);
		
		int spaces = (headerLength - (header.length()+delimiterLength)); 
		
		if ( spaces > 0) {
			for (int i = 0; i < spaces; i++) 
				sb.append(" ");
			sb.append(" " + delimiter + " ");
			sb.append(data);
		} else {
			sb.append(" " + delimiter + " ");
			sb.append(data);
		}
		
		if (carriageReturn)
			sb.append("\n");
		
		return sb.toString();
	}

	public static String getCurrentShortViewableDate() {
		Calendar cal = Calendar.getInstance();
		int curMonth = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		int year = cal.get(Calendar.YEAR);
		String month = "";
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        
        if (curMonth >= 0 && curMonth <= 11 ) 
            month = months[curMonth];
        
        return month + " " + day + ", " + year;
	}
	
	public static String getCurrentShortDateJavaStyle() {
		final String FORMAT = "MM-dd-yyyy";
		SimpleDateFormat sdf = new SimpleDateFormat(FORMAT);
		Calendar cal = Calendar.getInstance();
		java.util.Date d2 = cal.getTime();
		return sdf.format(d2);
	}
	
	public static String getCurrentShortDateMySQLStyle() {
		final String FORMAT = "yyyy-MM-dd";
		SimpleDateFormat sdf = new SimpleDateFormat(FORMAT);
		Calendar cal = Calendar.getInstance();
		java.util.Date d2 = cal.getTime();
		return sdf.format(d2);
	}
	
	public static String getCurrentShortDateMySQLStyle(final String FORMAT) {
		SimpleDateFormat sdf = new SimpleDateFormat(FORMAT);
		Calendar cal = Calendar.getInstance();
		java.util.Date d2 = cal.getTime();
		return sdf.format(d2);
	}
	
	public static String getCurrentTimeStamp() {
	    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	    Date now = new Date();
	    String strDate = sdfDate.format(now);
	    return strDate;
	}
	
	public long convertTimestampToLong(Timestamp timestamp) {
		return timestamp.getTime();
	}	
	
	public Timestamp convertLongToTimestamp(long time) {
		return new Timestamp(time);
	}
	
	public Timestamp convertStringToTimestamp(String timestampString) throws ParseException{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SS");
		Date parsedDate = sdf.parse(timestampString);
		return new Timestamp(parsedDate.getTime());
	}
	
	public static String getCurrentDateTimeMySQLStyle() {
		final String FORMAT = "yyyy-MM-dd hh:mm:ss";
		SimpleDateFormat sdf = new SimpleDateFormat(FORMAT);
		Calendar cal = Calendar.getInstance();
		java.util.Date d2 = cal.getTime();
		return sdf.format(d2);
	}
	
	public static File getAuditFolder(HttpServletRequest request, ServletContext servletContext) throws Exception{
		File logsDir = getTomcatLogsFolder(request, servletContext);
		File auditLogDir = new File(logsDir, "audit");
		
		if (!auditLogDir.exists())
			auditLogDir.mkdir();
		
		return auditLogDir;
	}
	
	public static File getRegionalAlertsLogFile(HttpServletRequest request, ServletContext servletContext) throws Exception {
		
		File logFolder = getTomcatLogsFolder(request, servletContext);		
		String todaysDate = getCurrentShortDateMySQLStyle();
		File alertsLogFile = new File(logFolder, "regionalalerts_log."+todaysDate+".txt");
		
		if (!alertsLogFile.exists())
			alertsLogFile.createNewFile();
			
		return alertsLogFile;
	}

	public static File getAuditFile(HttpServletRequest request, ServletContext servletContext) throws Exception {
		
		File auditFolder = getAuditFolder(request, servletContext);
		
		if (!auditFolder.exists())
			auditFolder.mkdir();
		
		String todaysDate = getCurrentShortDateMySQLStyle();
		File auditFile = new File(auditFolder, "localhost_audit_log."+todaysDate+".txt");
		
		if (!auditFile.exists())
			auditFile.createNewFile();
			
		return auditFile;
	}

	public static File getDatLogFile(HttpServletRequest request, ServletContext servletContext) throws Exception {
		
		File auditFolder = getAuditFolder(request, servletContext);
		
		if (!auditFolder.exists())
			auditFolder.mkdir();
		
		String todaysDate = getCurrentShortDateMySQLStyle();
		File auditFile = new File(auditFolder, "localhost_dat_log."+todaysDate+".txt");
		
		if (!auditFile.exists())
			auditFile.createNewFile();
			
		return auditFile;
	}

	public static String getClassShortName(String thisClass) {
		String shortName = "";
		
		if (thisClass == null)
			return shortName;
			
		if (thisClass != null) {
			int index = thisClass.lastIndexOf(".");
			if (index > 0) {
				shortName = thisClass.substring((index+1), thisClass.length()); 
			}
		}
		
		return shortName;
	}
	
	public String getStackTraceFromThrowable(Throwable e) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		return sw.toString(); 
	}

	public static String getCallersMethodName(Throwable e) {
		Throwable t = new Throwable(); 
		StackTraceElement[] elements = t.getStackTrace();
		return getCallersMethodName(elements);
	}
	
	public static String getCallersMethodName(StackTraceElement[] ste) {
		
		String classname = "";
		String methodName = ste[ste.length - 2].getMethodName();

		try {
			classname = (ste[ste.length - 2].getClassName() == null) ? "" : ste[ste.length - 2].getClassName();
			
			String[] parts = classname.split("\\.");
			
			if (parts.length > 1)
				classname = parts[parts.length-1];
			
			if (StringUtils.hasText(classname) && classname.indexOf("$") != -1)
				classname = classname.split("\\$")[0];
		
			methodName = classname +"::"+methodName+"()"; 
					
		} catch (Exception e) {
			Msu.warn("<getCallersMethodName> e.getMessage()="+e.getMessage());
		}
		
		return methodName;
	}
	
	public static String getMethodName(StackTraceElement[] ste) {
		return ste[ste.length - 1 - 0].getMethodName();
	}
	
	public static File getIPFile(ServletContext servletContext, String country) {
		String rootContext = servletContext.getRealPath("");
		File documentsDirectory = new File(new File(rootContext, "assets"), "documents");
		File ipFile = new File(documentsDirectory, "ips-"+country.toLowerCase()+".txt");
		return ipFile;
	}
	
	public static File getDocumentsDirectory(ServletContext servletContext) {
		String rootContext = servletContext.getRealPath("");
		return new File(new File(rootContext, "assets"), "documents");
	}

	public static File getDocumentsDirectory(HttpServletRequest request) {
		String rootContext = request.getServletContext().getRealPath("");
		return new File(new File(rootContext, "assets"), "documents");
	}
	
	public static File getBlackListedIPsFile(ServletContext servletContext) throws Exception {
		File tomcatFolder = getTomcatFolder(servletContext);
		File ipFile = new File(tomcatFolder, "blacklist.txt");
		if (!ipFile.exists()) {
			ipFile.createNewFile();
		}
		return ipFile;
	}
	
	public static File getBlackListedIPsFile(HttpServletRequest request) throws Exception {
		File tomcatFolder = getTomcatFolder(request);
		File ipFile = new File(tomcatFolder, "blacklist.txt");
		if (!ipFile.exists()) {
			ipFile.createNewFile();
		}
		return ipFile;
	}
	
//	public String getXml() throws Exception{
//		XStream xstream = new XStream();
//		xstream.alias("fooBean", FooBean.class);
//		FooBean fooBean = new FooBean("1.0", "Michael", "P", "Jackson", "666 Dream Lane", "HollyWood", "CA",
//				"90210", true);
//		ByteArrayOutputStream stream = new ByteArrayOutputStream();
//		OutputStreamWriter writer = new OutputStreamWriter(stream, "UTF-8");
//		HierarchicalStreamWriter xmlWriter = new PrettyPrintWriter(writer);
//		writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n");
//		xstream.marshal(fooBean, xmlWriter);
//		return new String(stream.toByteArray(), "UTF-8");
//	}
	
	public static byte[] hexToByte(String s) {
	    int len = s.length();
	    byte[] data = new byte[len / 2];
	    
	    for (int i = 0; i < len; i += 2) {
	        data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
	                             + Character.digit(s.charAt(i+1), 16));
	    }
	    
	    return data;
	}
	
	public static String byteToHex(byte buf[]) {
		StringBuffer strbuf = new StringBuffer(buf.length * 2);
		int i;

		for (i = 0; i < buf.length; i++) {
			if (((int) buf[i] & 0xff) < 0x10)
				strbuf.append("0");

			strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
		}

		return strbuf.toString();
	}

	public static File getResourcesFolder(ServletContext servletContext) {
		String rootContext = servletContext.getRealPath("");
		File resourcesFolder = new File(new File(rootContext, "WEB-INF"), "classes");		
		return resourcesFolder;
	}
	
	public static File getResourcesFolder(HttpServletRequest request) {
		String rootContext = request.getServletContext().getRealPath("");
		File resourcesFolder = new File(new File(rootContext, "WEB-INF"), "classes");		
		return resourcesFolder;
	}
	
	public static String getKey(HttpServletRequest request, ServletContext servletContext) throws Exception {
		
		String rootContext = null;
		
		if (request != null)
			rootContext = request.getServletContext().getRealPath("");
		else 
			rootContext = servletContext.getRealPath("");
		
		File keyFile = new File(new File(new File(rootContext, "WEB-INF"), "classes"), "key");
		String key = "";
		FileReader fr=null;
		BufferedReader br=null;
		try{
			fr = new FileReader(keyFile);
			br = new BufferedReader(fr);
			if (br != null)
				//if (br.readLine() != null)
					key = br.readLine();
		}finally{
			Helper.close(fr, FILE_READER);
			Helper.close(br, BUFFERED_READER);
		}
		return key;
	}
	
	public static String getAes(HttpServletRequest request, ServletContext servletContext, String password) throws Exception {
		
		String key = Helper.getKey(request, servletContext);
		
		if (password == null || password.equals("")){
			return "";
		} else if (!key.equals(password)){
			return "";
		}
		
		String rootContext = null;
		
		if (request != null)
			rootContext = request.getServletContext().getRealPath("");
		else 
			rootContext = servletContext.getRealPath("");
		
		File aes = new File(new File(new File(rootContext, "WEB-INF"), "classes"), "aes");
		String aesString = "";
		FileReader fr=null;
		BufferedReader br=null;
		try{
			fr = new FileReader(aes);
			br = new BufferedReader(fr);
			aesString = br.readLine();
		}finally{
			Helper.close(fr, FILE_READER);
			Helper.close(br, BUFFERED_READER);
		}
		return aesString;
	}
	
	public static void close(Object obj, String type) {
		
		if (obj == null)
			return;
		
		try{
			if (type.equalsIgnoreCase(BUFFERED_READER))
				((BufferedReader) obj).close();
			if (type.equalsIgnoreCase(FILE_READER))
				((FileReader) obj).close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	public static File getBatchFile(HttpServletRequest request, String fileName) {
		String rootContext = request.getServletContext().getRealPath("");
		return new File(new File(new File(new File(rootContext, "WEB-INF"), "classes"), "batch"),fileName);
	}
	
	public static String getClientIpAddr(HttpServletRequest request) {

		if (request == null)
			return "";

		String ip = request.getHeader("X-Forwarded-For");
		
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
			ip = request.getHeader("Proxy-Client-IP");

		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
			ip = request.getHeader("WL-Proxy-Client-IP");

		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
			ip = request.getHeader("HTTP_CLIENT_IP");

		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");

		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))
			ip = request.getRemoteAddr();

		ip = (ip.equals("0:0:0:0:0:0:0:1") ? "127.0.0.1" : ip);
		return ip;
	}

	public static File getTrackingApplication(HttpServletRequest request, String fileName) {
		String rootContext = request.getServletContext().getRealPath("");
		return new File(new File(new File(rootContext, "WEB-INF"), "classes"),fileName);
	}
	
//	public static String getUserIdentitySQLClause(Integer userType, Integer identity, String tableAlias) throws Exception {
//		
//		if (userType == null || identity == null)
//			return "";
//		
//		Type type = Type.fromInt(userType.intValue());
//		return getUserIdentitySQLClause(type, identity, tableAlias);
//	}
	
	/**
	 * <p>Returns SQL Clause for a given identity. Include period in table alias.<br/>
	 * Example response:   lo.mc_number = 310</p>
	 * @param user - the user to be inspected
	 * @param tableAlias - the prefix for a specified table. Null if not applicable.
	 * @return SQL Clause
	 * @throws Exception
	 */
//	public static String getUserIdentitySQLClause(Type userType, Integer identity, String tableAlias) throws Exception {
//		
//		if (identity == null || userType == null) 
//			return null;
//				
//		tableAlias = (tableAlias == null ?  "" : tableAlias+".");
//		
//		if (Type.CARRIER == userType) {
//			return(" "+tableAlias+"mc_number = " + identity.intValue() + " ");
//		} else if (Type.SHIPPER == userType ) {
//			return(" "+tableAlias+"shipper_id = " + identity.intValue() + " ");
//		} else if (Type.AGENT == userType ) {
//			return(" "+tableAlias+"agent_guid = " + identity.intValue() + " ");
//		} else if (Type.FACTORING_COMPANY == userType) {
//			return(" "+tableAlias+"factoring_id = " + identity.intValue() + " ");
//		} else {
//			return null;
//		}
//	}

	public static String getUrlFromRequest(HttpServletRequest request, boolean getBaseUrlOnly) {
		
		if (request == null)
			return "";
		
		String reqUri = (request.getRequestURI() == null) ? "" : request.getRequestURI().toString();
		reqUri = reqUri.replace("/gbtspring", "");
		
		if (reqUri.equals("")) {
			reqUri = "/";
		}
		
		String queryString = request.getQueryString();
		
		if (queryString != null && !queryString.isEmpty())
			reqUri += "?" + queryString;
		
		// Clean up any odd paths
		reqUri = reqUri.replace("///", "/");
		reqUri = reqUri.replace("//", "/");
		
		if (getBaseUrlOnly) {
			String [] urlList = reqUri.split("\\?");
			reqUri = urlList[0];
			reqUri = reqUri.replace("/gbtspring", "/");
			reqUri = reqUri.replace("//", "/");	
		}
		
		// Change 127.0.0.1 to localhost
		reqUri = reqUri.replace("127.0.0.1", "localhost");
		
		return reqUri;
	}

	public static Map<String, Object> getParameterMapFromRequest(HttpServletRequest request) {
		
		if (request == null)
			return new HashMap<String, Object>();
			
		Map<String, String[]> pm = request.getParameterMap();
	    Iterator<?> it = pm.entrySet().iterator();
	    Map<String, Object> params = new HashMap<String, Object>();
	    
	    while (it.hasNext()) {
	        Map.Entry<?,?> pairs = (Map.Entry<?,?>)it.next();
	        
	        if (pairs.getKey() instanceof String) {
	        	String key = (String)pairs.getKey();
	        	Object value = pairs.getValue();
        		params.put(key, value);
	        }
	    }
	    
	    return params;
	}
	
	public static String getParametersFromRequest(HttpServletRequest request) {
		
		if (request == null)
			return "";
			
		Map<String, String[]> pm = request.getParameterMap();
	    Iterator<?> it = pm.entrySet().iterator();
	    StringBuilder sb = new StringBuilder();
	    
	    while (it.hasNext()) {
	        Map.Entry<?,?> pairs = (Map.Entry<?,?>)it.next();
	        
	        if (sb.length() != 0)
	        	sb.append(", ");
	        
	        Object valueObject = pairs.getValue();
	        
	        if (valueObject instanceof String) {
	        	sb.append(pairs.getKey() + "=" + pairs.getValue());	
	        } else if (valueObject instanceof String[]) {
	        	String[] valueArray = (String[])pairs.getValue();
	        	
	        	if (valueArray != null && valueArray.length > 0) {
	        		sb.append(pairs.getKey() + "=");
	        		int index = 0;
	        		
		        	for (String value : valueArray) {
		        		sb.append((index == 0) ? "" : ",");
		        		sb.append(value);
		        		index+=1;
					}	
	        	}
	        }
	        
	        //sb.append(pairs.getKey() + "=" + pairs.getValue());
	    }
	    
	    return sb.toString();
	}
	
	public static List<String> getParameterKeyListFromRequest(HttpServletRequest request) {
		
		if (request == null)
			return new ArrayList<String>();
			
		Map<String, String[]> pm = request.getParameterMap();
	    Iterator<?> it = pm.entrySet().iterator();
	    List<String> paramKeyList = new ArrayList<String>();
	    
	    while (it.hasNext()) {
	        Map.Entry<?,?> pairs = (Map.Entry<?,?>)it.next();
	        
	        if (pairs.getKey() instanceof String) {
	        	String key = (String)pairs.getKey();
        		paramKeyList.add(key);
	        }
	    }
	    
	    return paramKeyList;
	}
	
	public static Map<String, Object> getParametersFromRequest(HttpServletRequest request, String startsWith) {
		
		if (request == null)
			return new HashMap<String, Object>();
			
		Object finalValue = "";
		Map<String, String[]> pm = request.getParameterMap();
	    Iterator<?> it = pm.entrySet().iterator();
	    Map<String, Object> params = new HashMap<String, Object>();
	    
	    while (it.hasNext()) {
	        Map.Entry<?,?> pairs = (Map.Entry<?,?>)it.next();
	        
	        if (pairs.getKey() instanceof String) {
	        	String key = (String)pairs.getKey();
	        	Object value = pairs.getValue();
	        
	        	if (value instanceof String[]) {
	        		String[] valueArray = (String[])value;
	        		String values = org.apache.commons.lang3.StringUtils.join(valueArray, ',');
	        		finalValue = values;
	        		//System.out.println(key+"="+values);
	        	} else {
	        		finalValue = value;
	        		//System.out.println(key+"="+value);
	        	}
	        	
	        	if (key != null && key.startsWith(startsWith))
	        		params.put(key, finalValue);

	        }
	    }
	    
	    return params;
	}
	
	public static Map<String, Object> getUniqueParametersFromRequest(HttpServletRequest request) {
		
		if (request == null)
			return new HashMap<String, Object>();
			
		Map<String, String[]> pm = request.getParameterMap();
	    Iterator<?> it = pm.entrySet().iterator();
	    Map<String, Object> params = new HashMap<String, Object>();
	    
	    while (it.hasNext()) {
	        Map.Entry<?,?> pairs = (Map.Entry<?,?>)it.next();
	        
	        if (pairs.getKey() instanceof String) {
	        	String key = (String)pairs.getKey();
	        	Object value = pairs.getValue();
	       		params.put(key, value);
	        }
	    }
	    
	    return params;
	}
	
	public static String objectToString(Object obj) throws Exception {
		// TODO: Check if the object is instanceof List or Map and add internal 
		//       List or Map components inside of curley brackets {}
		Class<?> clazz = obj.getClass();
		StringBuilder sb = new StringBuilder();
		int i = 0;
		sb.append("[");
		
		for (Field field : clazz.getDeclaredFields()) {
			field.setAccessible(true);
			String name = field.getName();
			Object value = field.get(obj);
			if (i != 0)
				sb.append(", ");
			sb.append(name + ":" + value);
			i+=1;
		}
		
		sb.append("]");
		return sb.toString();
	}
	

	/**
	 * <p>If any parameter in the array is null or empty, the check returns false</p>
	 * @param stringsToCheck array of string
	 * @return boolean
	 */
	public static boolean isNotEmpty(String[] stringsToCheck) {
		for (String string : stringsToCheck) {
			if (string == null || string.isEmpty())
				return false;
		}
		return true;
	}

	/**
	 * <p>If any (string or integer) parameter in the array is null or empty, the check returns false</p>
	 * @param stringsToCheck array of object (only Strings and Integers checked)
	 * @return boolean
	 */
	public static boolean isNotEmpty(Object[] objectsToCheck) {
		for (Object obj : objectsToCheck) {
			if (obj == null)
				return false;
			if (obj instanceof String) {
				if (((String) obj).isEmpty())
					return false;
			}
			// Do not check integer. 
			// An integer cannot be non-null and not have a value
		}
		return true;
	}
	
	/**
	 * <p>If any parameter in the array contains text, the check returns false</p>
	 * @param stringsToCheck array of string
	 * @return boolean
	 */
	public static boolean isEmpty(String[] stringsToCheck) {
		for (String string : stringsToCheck) {
			if (StringUtils.hasText(string))
				return false;
		}
		return true;
	}
	
	/**
	 * <p>If any parameter in the array is null, the check returns false</p>
	 * @param stringsToCheck array of string
	 * @return boolean
	 */
	public static boolean isNotNull(Object[] objectsToCheck) {
		for (Object obj : objectsToCheck) {
			if (obj == null)
				return false;
		}
		return true;
	}
	
	/**
	 * <p>If any parameter in the array is null or String values empty, the check returns true</p>
	 * @param stringsToCheck array of string
	 * @return boolean
	 */
	public static boolean isNullOrEmpty(Object[] objectsToCheck) {
		for (Object obj : objectsToCheck) {
			if (obj == null) {
				return true;
			} else if (obj instanceof String) {
				if (!StringUtils.hasText(((String)obj)))
					return true;
			}
		}
		
		return false;
	}

	public static boolean isNotNullOrEmpty(Object[] objectsToCheck) {
		for (Object obj : objectsToCheck) {
			if (obj == null) {
				return false;
			} else if (obj instanceof String) {
				if (!StringUtils.hasText(((String)obj)))
					return false;
			}
		}
		
		return true;
	}
	
	/**
	 * <p>If any parameter in the array is null, the check returns true</p>
	 * @param stringsToCheck array of string
	 * @return boolean
	 */
	public static boolean isNull(Object[] objectsToCheck) {
		for (Object obj : objectsToCheck) {
			if (obj == null)
				return true;
		}
		return false;
	}
	
	/**
	 * <p>If any parameter in the array is not numeric, the check returns true</p>
	 * @param stringsToCheck array of string
	 * @return boolean
	 */
	public static boolean isNotNumeric(Object[] objectsToCheck) {
		for (Object obj : objectsToCheck) {
			if (obj == null || isNotInteger(obj.toString()))
				return true;
		}
		return false;
	}
	
	/**
	 * <p>If any parameter in the array is not numeric, the check returns false</p>
	 * @param stringsToCheck array of string
	 * @return boolean
	 */
	public static boolean isNumeric(Object[] objectsToCheck) {
		for (Object obj : objectsToCheck) {
			if (obj == null || isNotInteger(obj.toString()))
				return false;
		}
		return true;
	}

	public static boolean isNotValidBigDecimal(Object[] objectsToCheck) {
		for (Object obj : objectsToCheck) {
			if (obj == null || isNotValidBigDecimal(obj.toString()))
				return true;
		}
		return false;
	}
	
	public static boolean isValidBigDecimal(Object[] objectsToCheck) {
		
		for (Object obj : objectsToCheck) {
			if (obj == null || isNotValidBigDecimal(obj.toString()))
				return false;
		}
		
		return true;
	}
	
	public static boolean isNotValidBigDecimal(String string) {
		try {
			new BigDecimal(string);
			return false;
		} catch (Exception e) {
			return true;
		}
	}

	public static boolean isValidBigDecimal(String string) {
		try {
			new BigDecimal(string);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public static boolean isNonZeroBigDecimal(BigDecimal value) {
		try {
			if (value == null)
				return false;
			
			if (value.compareTo(Msu.ZERO_DOLLARS_BD) == 1)
				return true;
			else
				return false;
			
		} catch (Exception e) {
			return false;
		}
	}
	
	public static boolean isCause(Class<? extends Throwable> expected, Throwable exc) {
		return expected.isInstance(exc) || (exc != null && isCause(expected, exc.getCause()));
	}

	public static long getBlackListedIpAddressesLastModified(HttpServletRequest request) throws Exception {
		return getBlackListedIPsFile(request).lastModified();
	}

	public static int getSubstringOccurences(String content, String substring) {
		int lastIndex = 0;
		int count = 0;

		while (lastIndex != -1) {
			lastIndex = content.indexOf(substring, lastIndex);
			if (lastIndex != -1) {
				count++;
				content = content.substring((lastIndex + substring.length()), content.length());
			}
		}
		return count;
	}
	
	public static String getStringBetweenFlags(String content, String startFlag, String endFlag) {
		
		String str = null;
		
		if (!isNotEmpty(new String[] {content, startFlag, endFlag}))
			return str;
					
		int index = content.indexOf(startFlag);
		
		if (index == -1 )
			return str;

		content = content.substring((index + startFlag.length()), content.length()); 
		int endIndex = content.indexOf(endFlag);
		
		if (endIndex == -1)
			return str;
		
		str = content.substring(0, endIndex);
			
		return str;
	}
	
	public static Timestamp calendarToTimestamp(Calendar cal) {
		Date date = cal.getTime();
		return new Timestamp(date.getTime());
	}

	public static String getStatusLiteralById(Integer statusId) {
		return Status.getStatusLiteral(statusId).replace("_", " ");
	}

//	public static Integer getIdentityByUser(User user) throws Exception {
//
//		Integer identity = null;
//		if (user.getMcNumber() != null)
//			identity = user.getMcNumber();
//		if (user.getShipperId() != null)
//			identity = user.getShipperId();
//		if (identity != null)
//			return identity;
//		else
//			throw new Exception("Cannot find user identity.");
//	}

	private static String getLanIpAddressByDeviceName(String deviceName) throws Exception{
		String lanIpAddress = null;
		try {
			Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
			
			while (interfaces.hasMoreElements()) {
				NetworkInterface currentInterface = interfaces.nextElement();
				
				if (currentInterface.toString().toLowerCase().contains(deviceName.toLowerCase())) {
					lanIpAddress = currentInterface.getInetAddresses().nextElement().getHostAddress();
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lanIpAddress;
	}
	
	public static String getLanIPAddress(HttpServletRequest request) {
		String lanIpAddress = null;
		File tomcatFolder = getTomcatFolder(request);
		try{
			File gbtPropertiesFile = new File(tomcatFolder,"Ophi.properties");
			Properties props = new Properties();
			props.load(new FileInputStream(gbtPropertiesFile));
			String networkDeviceName = props.getProperty("network.device.name");
			lanIpAddress = getLanIpAddressByDeviceName(networkDeviceName);			
		}catch(Exception e) {
			return null;
		}
		return lanIpAddress;
	}

	public static String getFilenameFromUrlPath(String path) {
		String filename = null;
				
		//int index = path.lastIndexOf("/");
		int index = path.lastIndexOf(File.separator);
		
		if (index > 0 && path.length() > index) {
			filename = path.substring(index+1, path.length()); 
		}
		
		return filename;	
	}

	public static Model removeAttributesFromModel(Model model, String[] attributes) {
		
		Map<String, Object> modelMap = model.asMap();
		Map<String, Object> newModelMap = model.asMap();
		
		Iterator<?> it = modelMap.entrySet().iterator();
		
		while (it.hasNext()) {
			Map.Entry<?,?> pairs = (Map.Entry<?,?>)it.next();
			
			boolean found = false;
			for (String attribute : attributes) {
				if (attribute != null && attribute.equalsIgnoreCase((String)pairs.getKey())){
					found = true;
					break;
				}
			}
			
			if (!found) {
				newModelMap.put((String) pairs.getKey(), pairs.getValue());
			}
	    }
		
		model.addAllAttributes(newModelMap);
		return model;
	}

	public static List<Integer> getIdsByMap(Map<Integer, BigDecimal> fundingMap) {
		
		Iterator<?> it = fundingMap.entrySet().iterator();
		List<Integer> list = new LinkedList<Integer>();
		
		while (it.hasNext())
			 list.add((Integer)((Map.Entry<?,?>)it.next()).getKey());
		
		return list;
	}
	
	public static String encodeQuotesOnString(String string) {
		try {
			string = string.replace("'", "%27");
			string = string.replace("\"", "%22");
			return string;
		} catch(NullPointerException npe) {
			return string;
		}
	}

	public static String getMySQLTime(String hourStr, String minuteStr, String meridian) {
		
		try {
			Integer hour = Integer.parseInt(hourStr);
			Integer minute = Integer.parseInt(minuteStr);
			
			if ("PM".equalsIgnoreCase(meridian))
				hour += 12;
			
			return hour.toString() + ":" + minute.toString();
		} catch (NumberFormatException e) {
			return null;
		}
	}

	public static String getHtmlEncodedString(String string) {
		string = string.replace("'", "&#039;");
		string = string.replace("\"", "&#034;");
		return string;
	}

	public static BigDecimal getCount(String count) {
		BigDecimal countBD = new BigDecimal("0");
		
		if (count == null)
			return countBD;
			
		try {
			countBD = new BigDecimal(count);	
		} catch (NumberFormatException nfe) {
			return countBD;
		}
		
		return countBD;
	}

	public static boolean timestampHasLength(Timestamp timestamp) {
		
		if (timestamp == null)
			return false;
			
		if (StringUtils.hasLength(timestamp.toString()))
			return true;
			
		return false;
	}
	
	public static void sortListAlphabetically(List<String> list) {		
		
		Collections.sort(list, new Comparator<String>() {
			@Override
			public int compare(String s1, String s2) {
				return Collator.getInstance().compare(s1, s2);
			}
		});
	}

	public static boolean isEmpty(Object object) throws Exception {
		
		for (Field field : object.getClass().getDeclaredFields()) {
			field.setAccessible(true); // You might want to set modifier to public first.			
			Object value = field.get(object);
			
		    if (value instanceof String) {
		    	if (!StringUtils.hasLength((String)value))
		    		return true;
		    } else if (value == null) {
		    	return true;
		    }
		}
		return false;
	}

	public static String commaSeparateCurrency(Object object) {		
		try {
			return commaSeparateDouble(object);
		} catch (Exception e) {
			return "";
		}
	}
	
	public static String commaSeparateDouble(Object object) {		
		try {
			String objectStr = object.toString();
			Double objectDouble = Double.parseDouble(objectStr);
			return String.format("%,.2f", objectDouble);
		} catch (Exception e) {
			return "";
		}
	}
	
	public static String commaSeparateNumber(Object object) {		
		try {
			String objectStr = object.toString();
			Double objectDouble = Double.parseDouble(objectStr);
			return NumberFormat.getIntegerInstance().format(objectDouble);
		} catch (Exception e) {
			return "";
		}
	}
	
	public static void printMapToStandardOut(Map<String, String> map) {
		Msu.debug("----------- Parameter Report ------------");
		
		for (Map.Entry<String, String> entry : map.entrySet())
			Msu.debug(entry.getKey() + "=" + entry.getValue());
		
		Msu.debug("-----------------------------------------");
	}

	public static void printObjectMapToStandardOut(Map<?, ?> map) {
		Msu.debug("----------- Parameter Report ------------");
		
		for (Map.Entry<?, ?> entry : map.entrySet())
			Msu.debug(entry.getKey() + "=" + entry.getValue());
		
		Msu.debug("-----------------------------------------");
	}

	public static void printAllRequestParametersToStandardOut(HttpServletRequest request, boolean restrictFieldLength) {
		
		if (request == null)
			return;

		Enumeration<String> params = request.getParameterNames();
		
		Msu.debug("----------- Parameter Report ------------");
		
		while (params.hasMoreElements()) {
			String paramName = (String) params.nextElement();
			String field = request.getParameter(paramName);
			
			if (StringUtils.hasText(field) && field.length() > 100 && restrictFieldLength) 
				field = field.substring(0,100) + "...";
				
			System.out.println("Attribute Name - " + paramName + ", Value - " + field);
		}
		
		Msu.debug("-----------------------------------------");
	}
	
	public static void printAllSessionVariablesToStandardOut(HttpServletRequest request, String message) {
		
		if (request == null || request.getSession() == null)
			return;
		
		HttpSession session = request.getSession();
		printAllSessionVariablesToStandardOut(session, message);
	}

	public static void printAllSessionVariablesToStandardOut(HttpSession session, String message) {
		
		if (session == null)
			return;
		
		Enumeration<String> enames = session.getAttributeNames();
		
		Msu.debug("------------ Session Report -------------");
		Msu.debug(message);
		
		while (enames.hasMoreElements()) {
			String key = (String) enames.nextElement();
			String value = "" + session.getAttribute(key);
			Msu.debug(key + ": " + value);
		}	
		
		Msu.debug("-----------------------------------------");		
	}
	
	public static void printSettingRequestParametersToStandardOut(HttpServletRequest request) {
		
		if (request == null)
			return;
		
		Enumeration<String> enParams = request.getParameterNames();
		Enumeration<String> enValues = null;
		Msu.debug("----------- Parameter Report ------------");
		
		while(enParams.hasMoreElements()) {
			String paramName = (String)enParams.nextElement();

			if (paramName.startsWith("_setting")) {
				enValues = request.getParameterNames();

				while(enValues.hasMoreElements()) {
					String valueName = (String)enValues.nextElement();
					Msu.debug(paramName + "=" + valueName);
				}
			}
		}
		
		Msu.debug("-----------------------------------------");
	}

	public static boolean notIntegers(String[] params) {
		try {
			for (String string : params) {
				Integer.parseInt(string);
			}
			
			return false;
		} catch (Exception e) {
			return true;
		}
	}

	public static Integer getIntIgnoreError(String s) {
		try {
			return Integer.parseInt(s);
		} catch (Exception ex) {
			return null;
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Map sortMapByValue(Map<Integer, String> map) {

		List list = new LinkedList(map.entrySet());
		Map result = new LinkedHashMap();

		Collections.sort(list, new Comparator() {
			public int compare(Object o1, Object o2) {
				return ((Comparable) ((Map.Entry) (o1)).getValue().toString().toLowerCase())
						.compareTo(((Map.Entry) (o2)).getValue().toString().toLowerCase());
			}
		});

		for (Iterator it = list.iterator(); it.hasNext();) {
			Map.Entry entry = (Map.Entry) it.next();
			result.put(entry.getKey(), entry.getValue());
		}

		return result;
	}

	@SuppressWarnings("rawtypes")
	public static void gc() {
		try {
			Object obj = new Object();
			WeakReference ref = new WeakReference<Object>(obj);
			obj = null;
			
			while (ref.get() != null) {
				System.gc();
			}			
		} catch (Exception e) {
			e.printStackTrace();
			e.getMessage();
		}
	}
	
	public static void sleep(Integer milliseconds) {
		try {
			if (milliseconds != null) {
				Thread.sleep(milliseconds);
			}
		} catch (Exception e) {
			/** do nothing */
		}
	}

	public static boolean isLong(String inLong) {
		try {
			if (!StringUtils.hasText(inLong))
				return false;
				
			Long.parseLong(inLong);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean hasItems(Object[] array) {
		
		if (array != null && array.length > 0)
			return true;
		
		return false;
	}
	
	public static boolean hasItems(List<?> list) {
		
		if (list != null && list.size() > 0)
			return true;
		
		return false;
	}

	public static boolean hasItems(Map<?,?> map) {
		
		if (map != null && map.size() > 0)
			return true;
		
		return false;
	}
	
	public static boolean isSizeZero(Collection<?>[] objects) {
		for (Object obj : objects) {
			if (obj == null) {
				return true;
			} else if (obj instanceof Collection<?>) {
				return ((Collection<?>)obj).size() == 0;
			} else if (obj instanceof Iterable<?>) {
				  return !((Iterable<?>)obj).iterator().hasNext();
			}
		}
		
		return false;
	}
	
	public static boolean isSizeZero(Object[] objects) {
		for (Object obj : objects) {
			if (obj == null) {
				return true;
			} else if (obj instanceof Collection<?>) {
				if (((Collection<?>)obj).size() == 0)
					return true;
			} else if (obj instanceof Iterable<?>) {
				if (!((Iterable<?>)obj).iterator().hasNext())
					return true;
			} else if (obj instanceof Map<?,?>) {
				if (((Map<?,?>)obj).size() == 0)
					return true;
			}
		}
		
		return false;
	}

	public static boolean valuesAreTheSame(Object obj1, Object obj2) {
		
		if (obj1 == null && obj2 == null)
			return true;
		
		if (obj1 == null && obj2 != null)
			return false;
		
		if (obj1 != null && obj2 == null)
			return false;
		
		if (!obj1.getClass().equals(obj2.getClass()))
			return false;		
		
		return obj1.equals(obj2);
	}

	public static boolean valuesDiffer(Object obj1, Object obj2) {
		return !valuesAreTheSame(obj1, obj2);
	}
	
	public static String getCompleteUrlWithUniqueParameters(HttpServletRequest request) {
		String queryString = request.getQueryString();
		String completeURL = request.getRequestURL().toString() + ((Msu.hasText(queryString)) ? ("?"+queryString) : "");
		
		if (completeURL.indexOf("?") > 0) {
			String baseURL = completeURL.substring(0, completeURL.indexOf("?"));
			String paramString = getUniqueParameterString(request);
			return baseURL + "?" + paramString;
		}
		
		return completeURL;
	}

	public static String getUniqueParameterString(HttpServletRequest request) {

		String queryString = request.getQueryString();
		String completeURL = request.getRequestURL().toString() + ((Msu.hasText(queryString)) ? ("?"+queryString) : "");

		// Process the params if we have them
		if (completeURL.indexOf("?") > 0) {
			StringBuilder sb = new StringBuilder();
			String paramString = completeURL.substring((completeURL.indexOf("?")+1), completeURL.length());
			String[] paramArray = paramString.split("&", -1);
			Map<String, Object> paramMap = new HashMap<String, Object>();
			
			// Loop though the key-value pairs and add them to the map
			for (String keyValueString : paramArray) {
				String[] keyValueArray = keyValueString.split("=", -1);
				
				if (keyValueArray.length == 2)
					paramMap.put(keyValueArray[0], keyValueArray[1]);
			}
			
			// Loop through the key value map and add them to the StringBuilder
			for (Map.Entry<String, Object> entry : paramMap.entrySet()) {
			    String key = entry.getKey();
			    Object value = entry.getValue();
				
			    if (sb.toString().length() > 0)
					sb.append("&");
				
			    sb.append(key+"="+value);
			}
			
			return sb.toString();
		}		
		
		return "";
	}
	
	public static boolean duplicateParametersExistOnRequest(HttpServletRequest request) {
		
		String queryString = request.getQueryString();
		String completeURL = request.getRequestURL().toString() + ((Msu.hasText(queryString)) ? ("?"+queryString) : "");
		
		if (completeURL.indexOf("?") > 0) {
			String paramString = completeURL.substring((completeURL.indexOf("?")+1), completeURL.length());
			String[] paramArray = paramString.split("&", -1);
			Map<String, Object> paramMap = new HashMap<String, Object>();
			
			for (String keyValueString : paramArray) {
				String[] keyValueArray = keyValueString.split("=", -1);
				
				if (keyValueArray.length == 2)
					paramMap.put(keyValueArray[0], keyValueArray[1]);
			}
			
			return paramMap.size() < paramArray.length;
		}
		
		return false;
	}

	public static void printAllHeaderDetailsToStandardOut(HttpServletRequest request) {
		
		Map<String, String> headerMap = getHeaderInformation(request);
		
		System.out.println("------------ Header Report --------------");
		
		for (Map.Entry<String, String> entry : headerMap.entrySet()) {
		    String key = entry.getKey();
		    String value = entry.getValue();
		    Msu.debug(key + " : " + value);
		}
		
		System.out.println("-----------------------------------------");
	}
	
    public static Map<String, String> getHeaderInformation(HttpServletRequest request) {

        Map<String, String> map = new HashMap<String, String>();
        Enumeration<String> headerNames = request.getHeaderNames();
        
        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            String value = request.getHeader(key);
            map.put(key, value);
        }

        return map;
    }

	public static String getErrorMessage(Exception e) {
		
		if (e == null)
			return "NULL";
		
		return e.getMessage();
	}

	public static int getMaxFileSize() {
		// If the size changes here, do not forget to update the key/value for "error.file.size.restriction"
		// in the following file: /src/main/resources/messages_en.properties
		return 3 * 5 * 1024 * 1024; // 15MB
	}

	public static String getMaxFileSizeString() {
		return "15 Megabytes";
	}
	
}
