package edu.missouristate.util.helpers;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class Test {

	public Test() {
		testBcrypt();
	}

	private void testBcrypt() {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		String str1 = "password";
		String enc1 = new BCryptPasswordEncoder().encode(str1);

		String str2 = "password";
		String enc2 = encoder.encode(str2);

		System.out.println("str1=" + str1);
		System.out.println("enc1=" + enc1);
		System.out.println("str2=" + str2);
		System.out.println("enc2=" + enc2);
		System.out.println("str1 ~ enc1: " + ((encoder.matches(str1, enc1)) ? "true" : "false"));
		System.out.println("str1 ~ enc2: " + ((encoder.matches(str1, enc2)) ? "true" : "false"));
		System.out.println("str2 ~ enc1: " + ((encoder.matches(str2, enc1)) ? "true" : "false"));
		System.out.println("str2 ~ enc2: " + ((encoder.matches(str2, enc2)) ? "true" : "false"));

	}

//	public static void main(String[] args) {
//		new Test();
//	}

}
