package edu.missouristate.util.helpers;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import org.apache.commons.beanutils.BeanUtils;

public class ObjectHelper {
	
	/**
	 * The easy way to copy object1 to object2!!
	 * @param obj1
	 * @param obj2
	 * @return
	 * @throws Exception
	 */
	public static Object copyObject(Object obj1, Object obj2) {
		
		try {
			if (obj1 == null || obj2 == null) {
				return obj2;
			}
			
			BeanUtils.copyProperties(obj1, obj2);
			
			return obj2;	
		} catch (Exception e) {
			e.printStackTrace();
			/** TODO Send Email to Administrators */
			return obj2;
		}
	}
			
	/**
	 * The hard way to copy object1 to object2!!
	 * This method copies obj1 into obj2 and returns obj2.
	 * This method assumes that:
	 *  - klazz, obj1, and obj2 are not null
	 *  - obj1 and obj2 are instances of klazz
	 *  - returns the updated version of obj2 (conditions met) 
	 * @param klazz
	 * @param obj1
	 * @param obj2
	 * @return
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	public static <T> Object copyObject(Class<T> klazz, Object obj1, Object obj2, boolean copyNullValues) throws Exception {
		
		// Make sure we do not have any null objects
		if (klazz == null || obj1 == null || obj2 == null) {
			return obj2;
		}
		
		// Make sure the objects are instances of the "Class" klazz 
		if (!klazz.isInstance(obj1) || !klazz.isInstance(obj1)) {
			return obj2;
		}
		
		//Field[] fields = getClassFields(klazz, false);
		
		//Object someObject = getItSomehow();
		for (Field field1 : obj1.getClass().getDeclaredFields()) {
		    field1.setAccessible(true);
		    Object object1Value = field1.get(obj1);
		    
		    // If the caller does not want to copy null values, move
		    // onto the next field.
		    if (object1Value == null && !copyNullValues) {
		    	continue;
		    }
		    
		    // Copy the values from obj1 to obj2 for the current field
		    for (Field field2 : obj2.getClass().getDeclaredFields()) {
		    	if (field1 == field2) {
		    		field2.setAccessible(true);
		    		field2.set(obj2, object1Value);
		    	}
			    
		    }
		}
		
		return obj2;
	}
	
    public static <T> Field[] getClassFields(Class<T> klazz, boolean printFields) {
        Field[] fields = klazz.getDeclaredFields();
        
        if (printFields) {
        	System.out.printf("%d fields:%n", fields.length);
            
            for (Field field : fields) {
                System.out.printf("%s %s %s%n",
                    Modifier.toString(field.getModifiers()),
                    field.getType().getSimpleName(),
                    field.getName()
                );
            }	
        }
        
        return fields;
    }

}
