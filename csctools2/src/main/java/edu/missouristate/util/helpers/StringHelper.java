package edu.missouristate.util.helpers;

import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.text.WordUtils;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import edu.missouristate.util.enums.Roles;
import edu.missouristate.util.enums.Status;
import edu.missouristate.util.enums.Type;

public class StringHelper {

	private static final String EQUALS = "equals";
	private static final String ENDS_WITH = "endsWith";
	private static final String CONTAINS = "contains";
	
	public static Integer parseInteger(String integerStr, boolean removeNonNumerics) {
		try {
			boolean isNegative = false;
					
			if (hasText(integerStr)) {
				integerStr = integerStr.trim();
				isNegative = integerStr.startsWith("-");
			}
			
			if (removeNonNumerics) {
				integerStr = removeNonNumericCharacters(integerStr);
				return (isNegative) ? (Integer.parseInt(integerStr) * -1) : Integer.parseInt(integerStr);	
			} else {
				return Integer.parseInt(integerStr);
			}
		} catch (Exception e) {
			return null;
		}
	}
	
	public static Integer parseInteger(String integerStr) {
		try {
			return parseInteger(integerStr, true);
		} catch (Exception e) {
			return null;
		}
	}
	
	private static final String IP_PATTERN = 
	        "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
	        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
	        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
	        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
	
	public static String getViewableStatusString(String statusStr) {
		
		if (statusStr == null)
			return statusStr;
		
		// Make string changes here
		
		return statusStr;
	}	
		
	public static String getViewableTypeString(String typeStr) {
		
		if (typeStr == null)
			return typeStr;
		
		typeStr = typeStr.replace("ACTION_CREDIT_CREDIT_ACCOUNT", "GBT_CREDIT_ADJUSTMENT");
		typeStr = typeStr.replace("ACTION_DEBIT_CREDIT_ACCOUNT", "GBT_CREDIT_ADJUSTMENT");
		typeStr = typeStr.replace("ADDRESS_FIRST_COME_FIRST_SERVER", "FIRST_COME_FIRST_SERVE");
		typeStr = typeStr.replace("ADDRESS_SHIPPER_SETS_APPOINTMENT_AFTER_MATCH", "GBT_WILL_SET_APPOINTMENT_AFTER_MATCH");
		typeStr = typeStr.replace("ADDRESS_CARRIER_SETS_APPOINTMENT_AFTER_MATCH", "CARRIER_MUST_SET_APPOINTMENT_AFTER_MATCH");
		typeStr = typeStr.replace("ADDRESS_APPOINTMENT_SET", "AN_APPOINTMENT_HAS_ALREADY_BEEN_SET");
		typeStr = typeStr.replace("CARRIER_ED_SETUP", "Started Set-Up Process");
		typeStr = typeStr.replace("CARRIER_ED_FIND_FREIGHT", "Find Freight/Submit Quote");
		typeStr = typeStr.replace("CARRIER_ED_COUNTER", "Respond to Counter Quotes");
		typeStr = typeStr.replace("CARRIER_ED_ESIGN", "E-sign Rate Confirmation");
		typeStr = typeStr.replace("CARRIER_ED_UPDATES", "Load Updates, Mark L and D");
		typeStr = typeStr.replace("CARRIER_ED_LOCATING", "Use Mobile Locating App");
		typeStr = typeStr.replace("CARRIER_ED_ACCESSORIALS", "Request Accessorials");
		typeStr = typeStr.replace("CARRIER_ED_POD", "Upload POD");
		typeStr = typeStr.replace("CARRIER_ED_TRANSFER_FUNDS", "Transfer Funds");
		typeStr = typeStr.replace("FACTORING_TRANSFER", "TRANSFER_TO_FACTORING_COMPANY");
		typeStr = typeStr.replace("PROMOTIONAL_CREDIT", "REFERRAL_CREDIT");
		typeStr = typeStr.replace("PAY_NET_SAVINGS", "SHIPMENT_CREDIT");
		typeStr = typeStr.replace("PAY_MATCHED_SHIPMENT", "SHIPMENT_PAYMENT");
		typeStr = typeStr.replace("POST", "SHIPMENT_ESCROW_PAYMENT");
		typeStr = typeStr.replace("FUND_LOAD", "SHIPMENT_ESCROW_PAYMENT");
		typeStr = typeStr.replace("CANCEL_SHIPMENT", "CANCELLED_SHIPMENT_PAYMENT");
		typeStr = typeStr.replace("CC_CREDIT", "WITHDRAWAL - CC");
		typeStr = typeStr.replace("ACH_CREDIT", "WITHDRAWAL - BANK");
		typeStr = typeStr.replace("CHECK_REQUEST", "WITHDRAWAL - CHECK");
		typeStr = typeStr.replace("WITHDRAW_BANK_FUNDS", "TRANSFER_TO_BANK_ACCOUNT");
		typeStr = typeStr.replace("WIRE_REQUEST", "WIRE_TRANSFER");
		typeStr = typeStr.replace("FREIGHT_GENERAL", "GENERAL_FREIGHT");
		typeStr = typeStr.replace("FREIGHT_BULK", "BULK_FREIGHT");
		typeStr = typeStr.replace("FREIGHT_VEHICLE", "VEHICLE");
		typeStr = typeStr.replace("FREIGHT_OTHER", "OTHER");
		typeStr = typeStr.replace("NOTE_COMM_PHONE", "PHONE_CALL");
		typeStr = typeStr.replace("NOTE_COMM_EMAIL", "EMAIL");
		typeStr = typeStr.replace("NOTE_COMM_FACE", "FACE_TO_FACE");
		typeStr = typeStr.replace("TX_", "");
		typeStr = typeStr.replace("ACTION_", "");
		typeStr = typeStr.replace("MSG_", "");
		typeStr = typeStr.replace("DC_", "");
		typeStr = typeStr.replace("MEMO_", "");
		typeStr = typeStr.replace("_", " ");
		typeStr = typeStr.replace(" LOAD ", " SHIPMENT ");
		
		typeStr = typeStr.toLowerCase();
		typeStr = WordUtils.capitalize(typeStr);
		
		typeStr = typeStr.replace("Gbt", "GBT");
		typeStr = typeStr.replace("Pod", "POD");
		typeStr = typeStr.replace("Freight/submit", "Freight/Submit");
		
		if ("Dc".equalsIgnoreCase(typeStr))
			typeStr = "DC";
		
		return typeStr;
	}
	
	private static final String IPV4PATTERN = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
			+ "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
			+ "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
			+ "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

	public static boolean isValidIPV4(final String ip) {
		Pattern pattern = Pattern.compile(IPV4PATTERN);
		Matcher matcher = pattern.matcher(ip);
		return matcher.matches();
	}

	public static boolean stringContainsAlphaOrUnderscore(String str) {
		return str.matches("[a-zA-Z_.]+");
	}

	public static String cleanSpecialCharacters(String str) {

		if (!StringUtils.hasLength(str))
			return str;

		str = str.replaceAll("–", "-");
		str = str.replaceAll("“", "\"");
		str = str.replaceAll("”", "\"");

		str = str.replaceAll("‘", "'");
		str = str.replaceAll("’", "'");
		str = str.replaceAll("′", "'");
		str = str.replaceAll("â€™", "'");

		str = str.replaceAll("&amp;", "&");

		return str;
	}
	
	public static String processIdList(String ids) {

		if (!StringUtils.hasLength(ids))
			return ids;

		if (ids.indexOf("-") == -1)
			return ids;

		String[] parts = ids.split("-");
		String value = "";
		int count = 0;

		for (String str : parts) {
			if (count != 0) {
				value += ",";
			}
			count += 1;
			value += str;
		}

		return value;
	}

	public static Integer stringToInt(String str) {
		Integer newInt = null;
		try {
			newInt = Integer.parseInt(str);
		} catch (Exception e) {
			return newInt;
		}
		return newInt;
	}

	public static String getStackTraceFromException(Throwable ex) {
		if (ex == null)
			return "";
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		ex.printStackTrace(pw);
		String stackTrace = sw.toString(); // stack trace as a string
		return stackTrace;
	}

	public static String getGbtStackTraceFromException(Throwable e) {

		if (e == null)
			return "";
		
		StackTraceElement[] elements = e.getStackTrace();
		List<String> errorLines = new ArrayList<String>();
		StringBuilder sb = new StringBuilder();
		
		for (StackTraceElement ste : elements) {
			String klass = ste.getClassName();
		    
		    if (hasText(klass) && klass.indexOf("com.gbt") >= 0) {
		    	Integer lineNumber = ste.getLineNumber();
			    String methodName = ste.getMethodName();
		    	String line = klass + " : " + methodName + "() -> Line " + lineNumber;
		    	
		    	if (hasText(klass) && klass.indexOf("$$") == -1)
		    		errorLines.add(line);
		    }
		}
		
		if (Helper.hasItems(errorLines))
			Collections.reverse(errorLines);
		
		for (String  line : errorLines) {
			if (hasText(line)) {
				line = line.replace("\r\n", "").replace("\n", "");
				sb.append(line + "<br/>\n");
			}
		}
	
		return sb.toString();
	}

	public static String getLastCharactersOfString(String str,
			int numberOfCharacters) {
		if (StringUtils.hasLength(str))
			return str.length() <= numberOfCharacters ? str : str.substring(str
					.length() - 4);
		return str;
	}

	public static synchronized String convertHTMLCodes(String string) {
		string = string.replace("&#032;", " ");
		string = string.replace("&#033;", "!");
		string = string.replace("&#034;", "\"");
		string = string.replace("&#035;", "#");
		string = string.replace("&#036;", "$");
		string = string.replace("&#037;", "%");
		string = string.replace("&#038;", "&");
		string = string.replace("&#039;", "'");
		string = string.replace("&#040;", "(");
		string = string.replace("&#041;", ")");
		string = string.replace("&#042;", "*");
		string = string.replace("&#043;", "+");
		string = string.replace("&#044;", ",");
		string = string.replace("&#045;", "-");
		string = string.replace("&#046;", ".");
		string = string.replace("&#047;", "/");
		string = string.replace("&#048;", "0");
		string = string.replace("&#049;", "1");
		string = string.replace("&#050;", "2");
		string = string.replace("&#051;", "3");
		string = string.replace("&#052;", "4");
		string = string.replace("&#053;", "5");
		string = string.replace("&#054;", "6");
		string = string.replace("&#055;", "7");
		string = string.replace("&#056;", "8");
		string = string.replace("&#057;", "9");
		string = string.replace("&#058;", ":");
		string = string.replace("&#059;", ";");
		string = string.replace("&#060;", "<");
		string = string.replace("&#061;", "=");
		string = string.replace("&#062;", ">");
		string = string.replace("&#063;", "?");
		string = string.replace("&#064;", "@");
		string = string.replace("&#065;", "A");
		string = string.replace("&#066;", "B");
		string = string.replace("&#067;", "C");
		string = string.replace("&#068;", "D");
		string = string.replace("&#069;", "E");
		string = string.replace("&#070;", "F");
		string = string.replace("&#071;", "G");
		string = string.replace("&#072;", "H");
		string = string.replace("&#073;", "I");
		string = string.replace("&#074;", "J");
		string = string.replace("&#075;", "K");
		string = string.replace("&#076;", "L");
		string = string.replace("&#077;", "M");
		string = string.replace("&#078;", "N");
		string = string.replace("&#079;", "O");
		string = string.replace("&#080;", "P");
		string = string.replace("&#081;", "Q");
		string = string.replace("&#082;", "R");
		string = string.replace("&#083;", "S");
		string = string.replace("&#084;", "T");
		string = string.replace("&#085;", "U");
		string = string.replace("&#086;", "V");
		string = string.replace("&#087;", "W");
		string = string.replace("&#088;", "X");
		string = string.replace("&#089;", "Y");
		string = string.replace("&#090;", "Z");
		string = string.replace("&#091;", "[");
		string = string.replace("&#092;", "\\");
		string = string.replace("&#093;", "]");
		string = string.replace("&#094;", "^");
		string = string.replace("&#095;", "_");
		string = string.replace("&#096;", "`");
		string = string.replace("&#097;", "a");
		string = string.replace("&#098;", "b");
		string = string.replace("&#099;", "c");
		string = string.replace("&#100;", "d");
		string = string.replace("&#101;", "e");
		string = string.replace("&#102;", "f");
		string = string.replace("&#103;", "g");
		string = string.replace("&#104;", "h");
		string = string.replace("&#105;", "i");
		string = string.replace("&#106;", "j");
		string = string.replace("&#107;", "k");
		string = string.replace("&#108;", "l");
		string = string.replace("&#109;", "m");
		string = string.replace("&#110;", "n");
		string = string.replace("&#111;", "o");
		string = string.replace("&#112;", "p");
		string = string.replace("&#113;", "q");
		string = string.replace("&#114;", "r");
		string = string.replace("&#115;", "s");
		string = string.replace("&#116;", "t");
		string = string.replace("&#117;", "u");
		string = string.replace("&#118;", "v");
		string = string.replace("&#119;", "w");
		string = string.replace("&#120;", "x");
		string = string.replace("&#121;", "y");
		string = string.replace("&#122;", "z");
		string = string.replace("&#123;", "{");
		string = string.replace("&#124;", "|");
		string = string.replace("&#125;", "}");
		string = string.replace("&#126;", "~");
		string = string.replace("&#160;", "");
		string = string.replace("&#161;", "¡");
		string = string.replace("&#162;", "¢");
		string = string.replace("&#163;", "£");
		string = string.replace("&#164;", "¤");
		string = string.replace("&#165;", "¥");
		string = string.replace("&#166;", "¦");
		string = string.replace("&#167;", "§");
		string = string.replace("&#168;", "¨");
		string = string.replace("&#169;", "©");
		string = string.replace("&#170;", "ª");
		string = string.replace("&#171;", "«");
		string = string.replace("&#172;", "¬");
		string = string.replace("&#173;", "");
		string = string.replace("&#174;", "®");
		string = string.replace("&#175;", "¯");
		string = string.replace("&#176;", "°");
		string = string.replace("&#177;", "±");
		string = string.replace("&#178;", "²");
		string = string.replace("&#179;", "³");
		string = string.replace("&#180;", "´");
		string = string.replace("&#181;", "µ");
		string = string.replace("&#182;", "¶");
		string = string.replace("&#183;", "·");
		string = string.replace("&#184;", "¸");
		string = string.replace("&#185;", "¹");
		string = string.replace("&#186;", "º");
		string = string.replace("&#187;", "»");
		string = string.replace("&#188;", "¼");
		string = string.replace("&#189;", "½");
		string = string.replace("&#190;", "¾");
		string = string.replace("&#191;", "¿");
		string = string.replace("&#192;", "À");
		string = string.replace("&#193;", "Á");
		string = string.replace("&#194;", "Â");
		string = string.replace("&#195;", "Ã");
		string = string.replace("&#196;", "Ä");
		string = string.replace("&#197;", "Å");
		string = string.replace("&#198;", "Æ");
		string = string.replace("&#199;", "Ç");
		string = string.replace("&#200;", "È");
		string = string.replace("&#201;", "É");
		string = string.replace("&#202;", "Ê");
		string = string.replace("&#203;", "Ë");
		string = string.replace("&#204;", "Ì");
		string = string.replace("&#205;", "Í");
		string = string.replace("&#206;", "Î");
		string = string.replace("&#207;", "Ï");
		string = string.replace("&#208;", "Ð");
		string = string.replace("&#209;", "Ñ");
		string = string.replace("&#210;", "Ò");
		string = string.replace("&#211;", "Ó");
		string = string.replace("&#212;", "Ô");
		string = string.replace("&#213;", "Õ");
		string = string.replace("&#214;", "Ö");
		string = string.replace("&#215;", "×");
		string = string.replace("&#216;", "Ø");
		string = string.replace("&#217;", "Ù");
		string = string.replace("&#218;", "Ú");
		string = string.replace("&#219;", "Û");
		string = string.replace("&#220;", "Ü");
		string = string.replace("&#221;", "Ý");
		string = string.replace("&#222;", "Þ");
		string = string.replace("&#223;", "ß");
		string = string.replace("&#224;", "à");
		string = string.replace("&#225;", "á");
		string = string.replace("&#226;", "â");
		string = string.replace("&#227;", "ã");
		string = string.replace("&#228;", "ä");
		string = string.replace("&#229;", "å");
		string = string.replace("&#230;", "æ");
		string = string.replace("&#231;", "ç");
		string = string.replace("&#232;", "è");
		string = string.replace("&#233;", "é");
		string = string.replace("&#234;", "ê");
		string = string.replace("&#235;", "ë");
		string = string.replace("&#236;", "ì");
		string = string.replace("&#237;", "í");
		string = string.replace("&#238;", "î");
		string = string.replace("&#239;", "ï");
		string = string.replace("&#240;", "ð");
		string = string.replace("&#241;", "ñ");
		string = string.replace("&#242;", "ò");
		string = string.replace("&#243;", "ó");
		string = string.replace("&#244;", "ô");
		string = string.replace("&#245;", "õ");
		string = string.replace("&#246;", "ö");
		string = string.replace("&#247;", "÷");
		string = string.replace("&#248;", "ø");
		string = string.replace("&#249;", "ù");
		string = string.replace("&#250;", "ú");
		string = string.replace("&#251;", "û");
		string = string.replace("&#252;", "ü");
		string = string.replace("&#253;", "ý");
		string = string.replace("&#254;", "þ");
		string = string.replace("&#255;", "ÿ");
		string = string.replace("&#338;", "Œ");
		string = string.replace("&#339;", "œ");
		string = string.replace("&#352;", "Š");
		string = string.replace("&#353;", "š");
		string = string.replace("&#376;", "Ÿ");
		string = string.replace("&#402;", "ƒ");
		string = string.replace("&#8211;", "–");
		string = string.replace("&#8212;", "—");
		string = string.replace("&#8216;", "'");
		string = string.replace("&#8217;", "'");
		string = string.replace("&#8218;", "‚");
		string = string.replace("&#8220;", "\"");
		string = string.replace("&#8221;", "\"");
		string = string.replace("&#8222;", "„");
		string = string.replace("&#8224;", "†");
		string = string.replace("&#8225;", "‡");
		string = string.replace("&#8226;", "•");
		string = string.replace("&#8230;", "…");
		string = string.replace("&#8240;", "‰");
		string = string.replace("&#8364;", "€");
		string = string.replace("&#8482;", "™");
		return string;
	}
	
	public static String getHtmlEncodedString(String string) {
		string = string.replace("'", "&#039;");
		string = string.replace("\"", "&#034;");
		return string;
	}

	// NOT TESTED YET (Should work nicely tho ~jlb 7/21/2016)
/*	public static String getUniqueDelimitedString(Object[] array) {
		
		if (array == null)
			return null;
		else if (array.length == 0)
			return "";
		
		Set<Object> objSet = new HashSet<Object>();
		
		for (Object obj : array)
			objSet.add(obj);
		
		if (objSet.size() > 0)
			return objSet.toString().replace("[", "").replace("]", "");
		
		return null;
	}*/
	
	public static String getDelimitedString(Object[] array) {

		StringBuilder sb = new StringBuilder();
		int count = 0;

		for (Object obj : array) {
			if (count != 0)
				sb.append(",");
			
			if (obj instanceof Status)
				sb.append(((Status)obj).getIntValue());
			else if (obj instanceof Type)
				sb.append(((Type)obj).getIntValue());
			else if (obj instanceof Roles)
				sb.append(((Roles)obj).getIntValue());
			else 
				sb.append(obj.toString());
			
			count += 1;
		}

		return sb.toString();
	}
	
	public static String getUniqueDelimitedString(List<?> list) {

		if (list == null)
			return null;
		else if (list.size() == 0)
			return "";
		
		Set<Object> objSet = new HashSet<Object>();
		
		for (Object obj : list)
			objSet.add(obj);
		
		if (objSet.size() > 0)
			return objSet.toString().replace("[", "").replace("]", "");
		
		return null;
	}
	
	public static String getDelimitedString(List<?> list) {

		StringBuilder sb = new StringBuilder();
		int count = 0;

		if (list == null)
			return "";
		
		for (Object obj : list) {
			
			if (obj == null)
				continue;
			
			if (count != 0)
				sb.append(",");
			
			if (obj instanceof Status)
				sb.append(((Status)obj).getIntValue());
			else if (obj instanceof Type)
				sb.append(((Type)obj).getIntValue());
			else if (obj instanceof Roles)
				sb.append(((Roles)obj).getIntValue());
			else 
				sb.append(obj.toString());
			
			count += 1;
		}

		return sb.toString();
	}

	public static String getDelimitedString(List<?> list, String delimiter) {

		StringBuilder sb = new StringBuilder();
		int count = 0;

		for (Object obj : list) {
			if (count != 0)
				sb.append(delimiter);
			sb.append(obj.toString());
			count += 1;
		}

		return sb.toString();
	}

	public static String capitalizeFirstLetterOfEachWord(String str,
			boolean capitalizeFully) {
		
		if (capitalizeFully)
			return WordUtils.capitalizeFully(str);
		else
			return WordUtils.capitalize(str);
	}

/*	public static String stripAllNonAlphaNumericChars(String input) {
		
		if (StringUtils.hasLength(input)) 	
			return input.replaceAll("[^A-Za-z0-9\\s]", "");
		
		return input;
	}*/
	
	public static String removeSpecialCharactersAndSpaces(String str) {
		
		if (!StringUtils.hasLength(str))
			return str;
		
		str = str.replace(" ","_");
		str = str.replace("!","");
		str = str.replace("@","");
		str = str.replace("#","");
		str = str.replace("$","");
		str = str.replace("%","");
		str = str.replace("^","");
		str = str.replace("&","");
		str = str.replace("*","");
		str = str.replace("(","");
		str = str.replace(")","");
		str = str.replace("=","");
		str = str.replace("+","");
		str = str.replace("<","");
		str = str.replace(">","");
		str = str.replace("'","");
		str = str.replace("\"","");
		str = str.replace("‘","");
		str = str.replace("’","");
		str = str.replace("“","");
		str = str.replace("”","");
		str = str.replace("`","");
		str = str.replace("~","");
		str = str.replace(",","");
		
		str = str.replace("_____", "_");
		str = str.replace("____", "_");
		str = str.replace("___", "_");
		str = str.replace("__", "_");
		
		return str;
	}

	public static String convertStreamToString(InputStream inputStream) {
		
		if (inputStream == null)
			return "";
		
		String convertedString = "";
		Scanner scanner = new Scanner(inputStream);
		
		try {
			scanner = scanner.useDelimiter("\\A");
			convertedString = scanner.hasNext() ? scanner.next() : "";
		} finally {
			scanner.close();
		}
		
		return convertedString;
	}
	
	public static boolean isValidIpAddress(final String ip) {
		
		if (!StringUtils.hasLength(ip))
			return false;
		
		Pattern pattern = Pattern.compile(IP_PATTERN);
		Matcher matcher = pattern.matcher(ip);
		return matcher.matches();
	}
	
	public static Long parseLong(String string) {
		
		if (string == null)
			return null;
		
		string = string.replaceAll("[^\\d]", "");
		
		try {
			return Long.parseLong(string);
		} catch (NumberFormatException e) {
			return null;
		}
	}
	
	public static String removeSpaces(String string) {
		
		if (string == null)
			return string;
		
		return string.replace(" ", "");
	}
	
	public static String removeNonAlphaNumericAndSpaces(String string) {
		
		return removeSpaces(removeNonAlphaNumericCharacters(string));
	}
	
	public static String removeNonAlphaNumericCharacters(String string) {
		
		if (StringUtils.hasText(string))
			return string.replaceAll("/[^A-Za-z0-9 ]/", "");
		
		return string;
	}
	
	public static String removeNonNumericCharacters(String string) {
		
		if (string == null)
			return null;
		
		return string.replaceAll("[^\\d]", "");
	}
	
	public static String removeNonDecimalCharacters(String string) {
		
		if (string == null)
			return null;
		
		return string.replaceAll("[^\\d.]", "");
	}

	public static boolean isLong(String num) {
		
		if (num != null) {
			try {
				Long.parseLong(num);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		
		return false;
	}
	
	public static boolean isInt(String num) {
		
		if (num != null) {
			try {
				Integer.parseInt(num);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		
		return false;
	}

	public static String separateCamelCaseString(String str) {		
		return str.replaceAll(
				String.format("%s|%s|%s",
			         "(?<=[A-Z])(?=[A-Z][a-z])",
			         "(?<=[^A-Z])(?=[A-Z])",
			         "(?<=[A-Za-z])(?=[^A-Za-z])"
			      ), " ");
	}

	public static String formatCurrencyHtml(BigDecimal amount) {
		
		StringBuilder html = new StringBuilder();
		String change = "00";
		String dollars = "";
		
		if (amount == null)
			return null;
		
		if (amount.toString().indexOf(".") == -1) {
			dollars = amount.toString();
		} else {
			dollars = amount.toBigInteger().toString();
			change = amount.remainder(BigDecimal.ONE).toString().replace("0.","");
		}

		html.append("<sup class=\"pricing-dollar-sign\">$</sup>");
		html.append(Helper.commaSeparateNumber(dollars));
		html.append("<span class=\"decimal\">.</span><sup class=\"pricing-cents\">");
		html.append(change);
		html.append("</sup>");
		
		return html.toString();
	}
	
	public static BigDecimal formatCurrency(BigDecimal amount) {
		if (amount == null)
			return null;
		
		if (amount.toString().indexOf(".") > 0)
			return amount;
		else 
			return new BigDecimal(amount.toString() + ".00");
	}
	
	public static boolean stringContains(String url, String[] array,
			boolean ignoreCase) {
		
		return assessStringComparison(url, array, ignoreCase, CONTAINS);
	}
	
	public static boolean stringEquals(String url, String[] array,
			boolean ignoreCase) {
		
		return assessStringComparison(url, array, ignoreCase, EQUALS);
	}
	
	public static boolean stringEndsWith(String url, String[] array,
			boolean ignoreCase) {
		
		return assessStringComparison(url, array, ignoreCase, ENDS_WITH);
	}

	private static boolean assessStringComparison(String url, String[] array,
			boolean ignoreCase, String assessmentType) {

		boolean assessment = false;

		if (!StringUtils.hasText(url) || array == null || array.length == 0)
			return false;

		if (ignoreCase) {
			url = url.toLowerCase();

			for (int i = 0; i < array.length; i++)
				if (StringUtils.hasText(array[i]))
					array[i] = array[i].toLowerCase();
		}

		for (String string : array) {
			switch (assessmentType) {
			case CONTAINS:
				if (url.contains(string))
					return true;
				break;
			case ENDS_WITH:
				if (url.endsWith(string))
					return true;
				break;
			case EQUALS:
				if (url.equals(string))
					return true;
				break;
			}
		}

		return assessment;
	}

//	@SuppressWarnings("unchecked")
//	public static List<Integer> convertJSONArrayToIntegerList(JSONArray jsonArray) {
//		 
//		if (jsonArray == null)
//			return new ArrayList<Integer>();
//		
//		Iterator<String> iterator = jsonArray.iterator();
//		List<Integer> dotList = new ArrayList<Integer>();
//		
//		while (iterator.hasNext())
//			dotList.add(Integer.valueOf(String.valueOf(iterator.next())));
//		
//		return dotList;
//	}
	
	public static String getStatusLiteralList(Integer[] statuses, String delimeter) {
		
		StringBuilder statusLiterals = new StringBuilder();
		
		for (int i=0; i < statuses.length; i++) {
			if (i != 0)
				statusLiterals.append(delimeter);
			statusLiterals.append(Status.getViewableStatusLiteral(statuses[i]));
		}
		
		return statusLiterals.toString();
	}

	public static List<Integer> getIntegerList(String delimitedString, String delimeter) {
		
		String[] tokens = delimitedString.split(delimeter, -1);
		List<Integer> integerList = new ArrayList<Integer>();
		
		for (int i=0; i < tokens.length; i++) {
			if (Helper.isInteger(tokens[i])) {
				integerList.add(Integer.parseInt(tokens[i]));	
			}
		}
		
		return integerList;
	}
	
	public static List<String> getStringList(String delimitedString, String delimeter) {
		
		String[] tokens = delimitedString.split(delimeter, -1);
		List<String> stringList = new ArrayList<String>();
		
		for (int i=0; i < tokens.length; i++)
			stringList.add(tokens[i]);
		
		return stringList;
	}
	
	public static String toUppercase(String str) {
		
		if (str == null)
			return null;
		
		return str.toUpperCase();
	}
	
	public static BigDecimal parseBigDecimal(String bigDecimalStr) {
		try {
			boolean negative = bigDecimalStr.startsWith("-");
			bigDecimalStr = removeNonDecimalCharacters(bigDecimalStr);
			bigDecimalStr = (negative && !bigDecimalStr.startsWith("-")) ? ("-" + bigDecimalStr) : bigDecimalStr;
			
			BigDecimal bd = new BigDecimal(bigDecimalStr);
			return bd;
		} catch (NumberFormatException nfe) {
			return null;
		}
	}

	public static String getMinutesAndSeconds(long time) {
		return String.format("%d min, %d sec",
				TimeUnit.MILLISECONDS.toMinutes(time),
				TimeUnit.MILLISECONDS.toSeconds(time)
						- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time)));
	}

	public static String pad(Object obj, int maxLength, String padding) {
		
		try {
			String text = (obj == null) ? null : obj.toString();
			StringBuilder sb = new StringBuilder();
			
			if (!StringUtils.hasLength(text) || padding == null)
				return sb.toString();
			
			sb.append(text);
			
			if (text.length() < maxLength) {
				for (int i = 0; i < (maxLength - text.length()); i++) {
					sb.append(padding);
				}
			}
			
			return sb.toString();
			
		} catch (Exception e) {
			return (obj == null) ? null : obj.toString();
		}
	}
	
	public static boolean firstCharacterIsANumber(String string) {
		boolean firstCharacterIsANumber = false;
		
		if (string != null && string.length() >= 1) {
			try {
				Integer.parseInt(Character.toString(string.charAt(0)));
				return true;
			} catch (NumberFormatException nfe) {
				return false;
			}
		}
		
		return firstCharacterIsANumber;
	}

	public static String getPrettyPrintJson(String jsonData, String host, String path) {
		try {			
			if (StringUtils.hasText(jsonData)) {
				Gson gson2 = new GsonBuilder().setPrettyPrinting().create();
				JsonParser jp = new JsonParser();
				JsonElement je = jp.parse(jsonData);
				String prettyJsonString = gson2.toJson(je);
				return prettyJsonString;	
			}
			
			return null;
		} catch (JsonSyntaxException jse) {
			return null;
		} catch (Exception e) {
			return null;
		}
	}
	
	public static void prettyPrintJson(String requestType, String jsonData, String host, String path) {
		try {
			System.out.println(requestType+"=");
			
			if ("REQUEST".equalsIgnoreCase(requestType))
				System.out.println(host + path);
			
			if (StringUtils.hasText(jsonData)) {
				Gson gson2 = new GsonBuilder().setPrettyPrinting().create();
				JsonParser jp = new JsonParser();
				JsonElement je = jp.parse(jsonData);
				String prettyJsonString = gson2.toJson(je);
				System.out.println(prettyJsonString);	
			}
			
			System.out.println("");
		} catch (JsonSyntaxException jse) {
			
			if ("REQUEST".equalsIgnoreCase(requestType))
				System.out.println("JSON Request is improperly formatted.");
			else
				System.out.println("JSON Response is improperly formatted.");
			
			System.out.println(jsonData);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error Printing JSON Request to Standard Out. e.getMessage()="+e.getMessage());
		}
	}

	/**
	 * This documents contains sibling names that are not consistent with Java's valid Bean declarations. As a result, 
	 * the sibling names have to be converted back to their original naming convention. ~jlb 8/10/15 
	 */
	public static String convertEdi990Document(String xml) {
		
		if (!StringUtils.hasText(xml))
			return xml;
		
		xml = xml.replace("Edi>", "edi>");
		xml = xml.replace("ansix12>", "ansi-x12>");
		xml = xml.replace("transaction990", "transaction-990");
		xml = xml.replace("B1segment>", "B1-segment>");
		xml = xml.replace("B11>", "B1-1>");
		xml = xml.replace("B12>", "B1-2>");
		xml = xml.replace("B13>", "B1-3>");
		xml = xml.replace("B14>", "B1-4>");
		xml = xml.replace("N9segment>", "N9-segment>");
		xml = xml.replace("N91>", "N9-1>");
		xml = xml.replace("N92>", "N9-2>");
		return xml;
	}

	public static String getPrettyPrintJson(String data) {
		try {
			//int spacesToIndentEachLevel = 2;
			//new JSONObject(jsonString).toString(spacesToIndentEachLevel);
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			JsonParser jp = new JsonParser();
			JsonElement je = jp.parse(data);
			String prettyJsonString = gson.toJson(je);
			return prettyJsonString;
		} catch (Exception e) {
			return data;
		}
	}
	
	public String getCellProviderLiteral(String providerId) {
		switch (providerId) {
		case ("01_@message.alltel.com"):
			return "Alltel";
		case ("02_@txt.att.net"):
			return "AT&T";
		case ("03_@myboostmobile.com"):
			return "Boost Mobile";
		case ("04_@messaging.sprintpcs.com"):
			return "Sprint";
		case ("05_@tmomail.net"):
			return "T-Mobile";
		case ("06_@vtext.com"):
			return "Verizon";
		case ("07_@vmobl.com"):
			return "Virgin Mobile";
		case ("08_@sms.3rivers.net"):
			return "3 River Wireless";
		case ("09_@paging.acswireless.com"):
			return "ACS Wireless";
		case ("10_@txt.bellmobility.ca"):
			return "Bell Canada";
		case ("11_@txt.bell.ca"):
			return "Bell Mobility (Canada)";
		case ("12_@blueskyfrog.com"):
			return "Bluegrass Cellular";
		case ("13_@bplmobile.com"):
			return "BPL Mobile";
		case ("14_@cwwsms.com"):
			return "Carolina West Wireless";
		case ("15_@mobile.celloneusa.com"):
			return "Cellular One";
		case ("16_@csouth1.com"):
			return "Cellular South";
		case ("17_@cwemail.com"):
			return "Centennial Wireless";
		case ("18_@messaging.centurytel.net"):
			return "CenturyTel";
		case ("19_@txt.att.net"):
			return "Cingular (Now AT&T)";
		case ("20_@msg.clearnet.com"):
			return "Clearnet";
		case ("21_@comcastpcs.textmsg.com"):
			return "Comcast";
		case ("22_@corrwireless.net"):
			return "Corr Wireless Communications";
		case ("23_@mobile.dobson.net"):
			return "Dobson";
		case ("24_@sms.edgewireless.com"):
			return "Edge Wireless";
		case ("25_@fido.ca"):
			return "Fido";
		case ("26_@sms.goldentele.com"):
			return "Golden Telecom";
		case ("27_@messaging.sprintpcs.com"):
			return "Helio";
		case ("28_@text.houstoncellular.net"):
			return "Houston Cellular";
		case ("29_@ideacellular.net"):
			return "Idea Cellular";
		case ("30_@ivctext.com"):
			return "Illinois Valley Cellular";
		case ("31_@inlandlink.com"):
			return "Inland Cellular Telephone";
		case ("32_@pagemci.com"):
			return "MCI";
		case ("33_@page.metrocall.com"):
			return "Metrocall";
		case ("34_@my2way.com"):
			return "Metrocall 2-way";
		case ("35_@mymetropcs.com"):
			return "Metro PCS";
		case ("36_@fido.ca"):
			return "Microcell";
		case ("37_@clearlydigital.com"):
			return "Midwest Wireless";
		case ("38_@mobilecomm.net"):
			return "Mobilcomm";
		case ("39_@text.mtsmobility.com"):
			return "MTS";
		case ("40_@messaging.nextel.com"):
			return "Nextel";
		case ("41_@onlinebeep.net"):
			return "OnlineBeep";
		case ("42_@pcsone.net"):
			return "PCS One";
		case ("43_@txt.bell.ca"):
			return "President's Choice";
		case ("44_@sms.pscel.com"):
			return "Public Service Cellular";
		case ("45_@qwestmp.com"):
			return "Qwest";
		case ("46_@pcs.rogers.com"):
			return "Rogers AT&T Wireless";
		case ("47_@pcs.rogers.com"):
			return "Rogers Canada";
		case ("48_@email.swbw.com"):
			return "Southwestern Bell";
		case ("49_@tms.suncom.com"):
			return "Sumcom";
		case ("50_@mobile.surewest.com"):
			return "Surewest Communicaitons";
		case ("51_@msg.telus.com"):
			return "Telus";
		case ("52_@txt.att.net"):
			return "Tracfone";
		case ("53_@tms.suncom.com"):
			return "Triton";
		case ("54_@utext.com"):
			return "Unicel";
		case ("55_@email.uscc.net"):
			return "US Cellular";
		case ("56_@uswestdatamail.com"):
			return "US West";
		case ("57_@vmobile.ca"):
			return "Virgin Mobile Canada";
		case ("58_@sms.wcc.net"):
			return "West Central Wireless";
		case ("59_@cellularonewest.com"):
			return "Western Wireless";
		default:
			return "Unknown";
		}
	}

	public static String formatPhoneNumber(String phoneNumber) {
		
		try {
			phoneNumber = removeNonNumericCharacters(phoneNumber);
			
			if (!StringUtils.hasText(phoneNumber))
				return "";			
			
			if (phoneNumber.length() != 10)
				return phoneNumber;
			
			return String.format("%s-%s-%s", phoneNumber.substring(0, 3), phoneNumber.substring(3, 6), 
					phoneNumber.substring(6, 10));
			
		} catch (Exception e) {
			return null;
		}
		
	}
	
	public static String getString(String[] stringArray) {
		
		if (stringArray == null || !(stringArray instanceof String[]))
			return null;
    	
		StringBuilder sb = new StringBuilder();
		
    	for (String str : stringArray)
			sb.append(str);
    	
    	return sb.toString();
	}
	
	public static String convertNumericMonthToAbbreviation(String month) {
		switch(month) {
		case "01": return "Jan";
		case "1": return "Jan";
		case "02": return "Feb";
		case "2": return "Feb";
		case "03": return "Mar";
		case "3": return "Mar";
		case "04": return "Apr";
		case "4": return "Apr";
		case "05": return "May";
		case "5": return "May";
		case "06": return "Jun";
		case "6": return "Jun";
		case "07": return "Jul";
		case "7": return "Jul";		
		case "08": return "Aug";
		case "8": return "Aug";
		case "09": return "Sep";
		case "9": return "Sep";
		case "10": return "Oct";
		case "11": return "Nov";
		case "12": return "Dec";
		default: return "";
		}
	}
	
	public static String convertNumericMonthToAbbreviation(int month) {
		switch(month) {
		case 1: return "Jan";
		case 2: return "Feb";
		case 3: return "Mar";
		case 4: return "Apr";
		case 5: return "May";
		case 6: return "Jun";
		case 7: return "Jul";		
		case 8: return "Aug";
		case 9: return "Sep";
		case 10: return "Oct";
		case 11: return "Nov";
		case 12: return "Dec";
		default: return "";
		}
	}

	public static String getTwoDigitYear(Integer year) {
		
		if (year == null)
			return "";
		
		String yearStr = String.valueOf(year);
		
		if (yearStr.length() == 4)
			return yearStr.substring(2, yearStr.length());
		else
			return "";
	}

	public static String parseZipCode(String zip) {
		
		if (!StringUtils.hasText(zip))
			return zip;
		
		zip = zip.replace("-", "").replace(" ", "");
		
		if (StringHelper.isLong(zip) && zip.length() >= 5) {
			return zip.substring(0,5);
		} else {
			return zip;
		}
	}

	/**
	 * This documents contains sibling names that are not consistent with Java's valid Bean declarations. As a result, 
	 * the sibling names have to be converted back to their original naming convention. ~jlb 2/16/15 
	 */
	public static String convertEdi214Document(String xml) {
		
		if (!StringUtils.hasText(xml))
			return xml;
		
		xml = xml.replace("<com.gbt.beans.edi.EDI214.N10100loop>", "");
		xml = xml.replace("</com.gbt.beans.edi.EDI214.N10100loop>", "");
		xml = xml.replace("<com.gbt.beans.edi.EDI214.LX0200loop>", "");
		xml = xml.replace("</com.gbt.beans.edi.EDI214.LX0200loop>", "");
		
		xml = xml.replace("ansix12","ansi-x12");
		xml = xml.replace("AT70205loop","AT7-0205-loop");
		xml = xml.replace("AT81","AT8-1");
		xml = xml.replace("AT82","AT8-2");
		xml = xml.replace("AT83","AT8-3");
		xml = xml.replace("AT84","AT8-4");
		xml = xml.replace("AT8segment","AT8-segment");
		xml = xml.replace("B10segment","B10-segment");
		xml = xml.replace("B101","B10-1");
		xml = xml.replace("B102","B10-2");
		xml = xml.replace("B103","B10-3");
		xml = xml.replace("Edi>", "edi>");
		xml = xml.replace("L111","L11-1");
		xml = xml.replace("L112","L11-2");
		xml = xml.replace("L113","L11-3");
		xml = xml.replace("L11segment","L11-segment");
		xml = xml.replace("LX0200loop","LX-0200-loop");
		xml = xml.replace("LX1","LX-1");
		xml = xml.replace("LXsegment","LX-segment");
		xml = xml.replace("MS11","MS1-1");
		xml = xml.replace("MS12","MS1-2");
		xml = xml.replace("MS1segment","MS1-segment");
		xml = xml.replace("MS21","MS2-1");
		xml = xml.replace("MS22","MS2-2");
		xml = xml.replace("MS23","MS2-3");
		xml = xml.replace("MS2segment","MS2-segment");
		xml = xml.replace("N10100loop","N1-0100-loop");
		xml = xml.replace("N11","N1-1");
		xml = xml.replace("N12","N1-2");
		xml = xml.replace("N13","N1-3");
		xml = xml.replace("N14","N1-4");
		xml = xml.replace("N1segment","N1-segment");
		xml = xml.replace("N31","N3-1");
		xml = xml.replace("N3segment","N3-segment");
		xml = xml.replace("N41","N4-1");
		xml = xml.replace("N42","N4-2");
		xml = xml.replace("N43","N4-3");
		xml = xml.replace("N4segment","N4-segment");
		xml = xml.replace("transaction214","transaction-214");
		
		// Remove empty lines
		xml = xml.replaceAll("(?m)^[ \t]*\r?\n", "");
		
		return xml;
	}

	public static String addHtmlLineBreakOnText(String str) {
		
		if (StringUtils.hasText(str))
			str+="<br/>";
		
		return str;
	}

	public static String formatStandardHtmlCurrency(BigDecimal amount) {
		
		String amountStr = Helper.commaSeparateCurrency(amount);
		
		if (amountStr != null)
			return "$"+amountStr;
		
		return "";
	}

	public static String removeParamsFromUrl(String url) {
		
		if (url == null)
			return null;
		
		if (url.indexOf("?") > 0)
			return url.substring(0, url.indexOf("?"));
		
		return url;
	}

	public static String camelCaseToUnderscore(String camelCase) {
		if (StringUtils.hasText(camelCase)) {
			String regex = "([a-z])([A-Z]+)";
		    String replacement = "$1_$2";
		    String underscore = camelCase.replaceAll(regex, replacement).toLowerCase();
		    return underscore;	
		} else {
			return camelCase;
		}
	}

	public static boolean hasText(String str) {
		return StringUtils.hasText(str);
	}

	public static boolean firstStringContainsSecondString(String firstString, String secondString, boolean clean, boolean caseSensitive) {
		
		boolean contains = false;
		
		if (hasText(firstString) && hasText(secondString)) {
			
			if (clean) {
				firstString = firstString.replace("  ", " ");
				secondString = secondString.replace("  ", " ");
			}
			
			if (!caseSensitive) {
				firstString = firstString.toLowerCase();
				secondString = secondString.toLowerCase();
			}
			
			int index = firstString.indexOf(secondString);
			return index >= 0;
		}
		
		return contains;		
	}

	public static String emptyForNull(Object obj) {
		if (obj == null) {
			return "";
		} else {
			return String.valueOf(obj);
		}
	}
	
	public static String emptyForNull(Object obj, String replacement) {
		if (obj == null) {
			return replacement;
		} else {
			return String.valueOf(obj);
		}
	}
	
	public static CharSequence emptyForNull(String str) {
		return (str == null) ? "" : str;	
	}

	/**
	 * This method returns a NON-ZERO based month representation of the string
	 * @param str
	 * @return
	 */
	public static Integer convertStringToIntegerMonth(String str) {
		
		if (!hasText(str))
			return null;
		
		if (Helper.isInteger(str)) {
			Integer mon = Integer.parseInt(str); 
					
			if (mon >= 1 && mon <= 12)
				return mon;
			else
				return null;
		}
		
		str = str.toLowerCase().trim();
		
		switch(str) {
		case "jan": return 1;
		case "january": return 1;
		case "feb": return 2;
		case "february": return 2;
		case "mar": return 3;
		case "march": return 3;
		case "apr": return 4;
		case "april": return 4;
		case "may": return 5;
		case "jun": return 6;
		case "june": return 6;
		case "jul": return 7;
		case "july": return 7;
		case "aug": return 8;
		case "august": return 8;
		case "sep": return 9;
		case "september": return 9;
		case "oct": return 10;
		case "october": return 10;
		case "nov": return 11;
		case "november": return 11;
		case "dec": return 12;
		case "december": return 12;
		}
		
		return null;
	}

	public static Integer convertStringToInteger(String str) {
		
		if (!hasText(str))
			return null;
		
		str = str.trim().replace(",", "").replace("$", "");
		
		if (str.indexOf(".") > 0)
			str = str.substring(0, str.indexOf("."));
		
		if (Helper.isInteger(str))
			return Integer.parseInt(str);
		
		return null;
	}

	public static Integer convertStringToInteger(String str, char[] removeCharsArray) {
		
		boolean badCharFound = false;
		
		if (!hasText(str))
			return null;
		
		str = str.trim();
		
		if (removeCharsArray != null && removeCharsArray.length > 0) {
			StringBuilder sb = new StringBuilder();
			
			char[] charArry = str.toCharArray();
			
			for (char inChar : charArry) {
				
				badCharFound = false;
						
				if (!Helper.isInteger(inChar)) {
					for (char badChar : removeCharsArray) {
						if (inChar == badChar) {
							badCharFound = true;
							break;
						}
					}
				}
				
				if (!badCharFound)
					sb.append(inChar);
			}
			
			str = sb.toString();
		}
		
		if (str.indexOf(".") > 0)
			str = str.substring(0, str.indexOf("."));
		
		if (Helper.isInteger(str))
			return Integer.parseInt(str);
		
		return null;
	}
	
	public static String getValidTrailerType(String trailerType) {
		
		if (!hasText(trailerType))
			return trailerType;
		
		String tt = new String(trailerType).toLowerCase();
		
		if (tt.contains("double") && tt.contains("drop")) {
			return "double drop deck";
		} else if (tt.contains("flatbed")) {
			return "flatbed";
		} else if (tt.contains("gooseneck")) {
			return "removable gooseneck";
		} else if (tt.contains("reefer") || tt.contains("refrig")) {
			return "reefer";
		} else if (tt.equals("53' van") || tt.equals("53 van")) {
			return "van";
		}
		
		return trailerType;
	}

	public static String getTrimmedString(String string, Integer length, Boolean addEllipses) {
		
		if (!hasText(string) || length == null || addEllipses == null && length > 0)
			return string;
		
    	if (hasText(string) && string.length() > length) {
   			string = string.substring(0, length);
    		
    		if (addEllipses == true)
    			string += "...";
    	}
    	
    	return string;
	}

	/* Not Tested */
/*	public static Integer getZeroBasedMonth(String month) {
		
		Integer zeroBasedMonth = null;
		
		if (hasText(month) && (month.length() == 1 || month.length() == 2)) {
			
			if (month.length() == 2 && Helper.isInteger(month.charAt(0)) && !"0".equals(month))
				return Integer.parseInt(month) -1;
			
			if (month.length() == 2 && Helper.isInteger(month.charAt(0)) && Helper.isInteger(month.charAt(0))) {
				if ('0' == month.charAt(0) && '0' != month.charAt(0)) 
					return Integer.parseInt(month.substring(1, 2)) -1;
				else if ('0' != month.charAt(0))
					return Integer.parseInt(month) -1;					
			}
		}
		
		return zeroBasedMonth;
	}*/

	public static boolean isEqual(String stringOne, String stringTwo, boolean nullsAreEqual, boolean ignoreCase,
			boolean trimStrings) {
		
		if (stringOne == null && stringTwo == null)
			return nullsAreEqual;
		
		if (stringOne == null && stringTwo != null)
			return false;
		
		if (stringOne != null && stringTwo == null)
			return false;
	
		if (trimStrings) {
			stringOne = stringOne.trim();
			stringTwo = stringTwo.trim();
		}
		
		if (ignoreCase)
			return stringOne.equalsIgnoreCase(stringTwo);
		else
			return stringOne.equals(stringTwo);
	}

	public static String convertNullStringToEmptyString(String str, boolean addDoubleQuotes) {
		
		String from = "null";
		String to = (addDoubleQuotes) ? "\"\"" : "";
		
		if (hasText(str))
			str = str.replaceAll(from, to);
		
		return str;
	}
	
	public static StringBuilder convertNullStringToEmptyString(StringBuilder sb, boolean addDoubleQuotes) {
		String from = "null";
		String to = (addDoubleQuotes) ? "\"\"" : "";
	    int index = sb.indexOf(from);
	    
	    while (index != -1) {
	    	sb.replace(index, index + from.length(), to);
	        index += to.length();
	        index = sb.indexOf(from, index);
	    }

	    return sb;
	}

	public static String addBreaks(String message) {
		
		if (hasText(message))
			message = message.replace("\n", "\n<br/>");
		
		return message;
	}
	
    public static Integer getLevenshteinDistance(String string1, String string2, boolean ignoreCase) {
    	
    	if (ignoreCase) {
    		string1 = string1.toLowerCase();
            string2 = string2.toLowerCase();	
    	}
        
        int [] costs = new int [string2.length() + 1];
        
        for (int j = 0; j < costs.length; j++)
            costs[j] = j;
        
        for (int i = 1; i <= string1.length(); i++) {
            costs[0] = i;
            int nw = i - 1;
            
            for (int j = 1; j <= string2.length(); j++) {
                int cj = Math.min(1 + Math.min(costs[j], costs[j - 1]), string1.charAt(i - 1) == string2.charAt(j - 1) ? nw : nw + 1);
                nw = costs[j];
                costs[j] = cj;
            }
        }
        
        return costs[string2.length()];
    }
    
    public static BigDecimal getLevenshteinPercentage(String string1, String string2, boolean ignoreCase, int precision) {
    	
    	if (string1 != null && string1.length() == 0 && string2 != null && string2.length() == 0)
    		return new BigDecimal("100").setScale(precision);
    	
    	if (!hasText(string1) || !hasText(string2))
    		return new BigDecimal("0").setScale(precision);
    	
    	BigDecimal maxLength = new BigDecimal(Math.max(string1.length(), string2.length()));
    	BigDecimal distance = new BigDecimal(getLevenshteinDistance(string1, string2, ignoreCase));
    	BigDecimal ratio = distance.divide(maxLength, precision, BigDecimal.ROUND_HALF_UP);
    	BigDecimal percentage = new BigDecimal("1").subtract(ratio).multiply(new BigDecimal("100"));
    	return percentage;
    }

//	public static boolean stringContainsAnyItemInArray(String string, String[] array) {
//		
//		if (!hasText(string) || !Helper.hasItems(array))
//			return false;
//		
//		return org.apache.commons.lang.StringUtils.indexOfAny(string, array) != -1;
//	}

/*	public static String correctAddress(String addr) {
		
		if (hasText(addr)) {
			String[] parts = addr.split(" ");
			
			if (parts.length == 1) {
				if (addr.toLowerCase().startsWith("tbd"))
					return "TBD";
			}
		}
		
		return addr;
	}*/
	
/*	public static String correctTbd(String alias) {
		
		if (hasText(alias)) {
			String[] parts = alias.split(" ");
			
			if (parts.length == 1) {
				if (alias.toLowerCase().startsWith("tbd"))
					return "TBD";
			}
		}
		
		return alias;
	}*/
	
}
