package edu.missouristate.util.helpers;

import java.util.List;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityHelper {

	/**
	 * USER HAS AUTHORITY
	 * @param securityContext
	 * @param authority
	 * @return
	 */
	public static boolean hasAuthority(SecurityContext securityContext, String authority) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	
    	if (auth != null && auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals(authority))) {
    		return true;
    	}
		
		return false;
	}

	/**
	 * Get Username (email address)
	 * @param securityContext
	 * @return
	 */
	public static String getUsername(SecurityContext securityContext) {
		String username = "";
		
		if (securityContext != null && securityContext.getAuthentication() != null) {
			Authentication auth = securityContext.getAuthentication();
			
			if (auth != null && !(auth instanceof AnonymousAuthenticationToken)) {
			    return auth.getName();
			}
		}
		
		return username;
	}

	/**
	 * Has Any Authority
	 * @param securityContext
	 * @param list
	 * @return
	 */
	public static boolean hasAnyAuthority(SecurityContext securityContext, List<String> list) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if (auth == null) {
			return false;
		}

		for (String authority : list) {
			if (auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals(authority))) {
				return true;
			}
		}

		return false;
	}

}
