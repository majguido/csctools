package edu.missouristate.controllers;

import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;

@ControllerAdvice
public class BaseController implements ErrorController {

    @InitBinder
    public void initBinder(final WebDataBinder binder) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @ExceptionHandler
    public String resolveException(final Exception ex,final HttpServletRequest req, 
    		final HttpServletResponse response, HttpSession session, Model model) {
    	
    	if (ex instanceof AccessDeniedException) {
    		return "redirect:/login";
    	}
    	
    	handleException(ex, response, session, model);
        return "error";
    }
    
	public void handleException(Exception ex, HttpServletResponse response, HttpSession session, Model model) {
		
        // This is to make sure Spring Security handles this error
        final int MAX_BUILDER_SIZE = 256;
        final StringBuilder trace = new StringBuilder(MAX_BUILDER_SIZE).append("Unhandled exception:\r\n").append(ex.toString());
        
        //ex.printStackTrace();
        
        if (!(ex instanceof AccessDeniedException)) {
            for (final StackTraceElement ste : ex.getStackTrace()) {
                trace.append("\r\n\t").append(ste.toString());
            }

            System.out.println(trace.toString());
        }        

        String message = null;
        
		if (ex != null) {
			if (ex instanceof FileNotFoundException) {
				System.out.println("Found file not found exception");
			} else if (ex instanceof NullPointerException) {
				System.out.println("Found null pointer exception");
			} else if (ex instanceof AccessDeniedException) {
				//System.out.println("Found access denied exception");
				message = "Sorry, you are trying to load a page that you do not have access to. " + 
						"If you believe that you should have this access, please contact the program office for resolution.";
			} else if (ex instanceof NumberFormatException) {
				System.out.println("Found number format exception");
			} else if (ex instanceof IllegalArgumentException) {
				System.out.println("Found illegal argument exception");
			} else if (ex instanceof RuntimeException) {
				System.out.println("Found runtime exception");
			} else if (ex instanceof IllegalStateException) {
				System.out.println("Found illegal state exception");
			} else if (ex instanceof NoSuchMethodException) {
				System.out.println("Found no such method exception");
			} else if (ex instanceof Exception) {
				System.out.println("Found exception");
			} else if (ex instanceof ParseException) {
				System.out.println("Found parse exception");
			} else if (ex instanceof InvocationTargetException) {
				System.out.println("Found invocation target exception");
			}
		}

		message = (message != null) ? message : 
			"Sorry, there was an internal error. Webmasters were alerted and will look into the error";
		model.addAttribute("message", message);
    }

	public void setMessageOnSession(HttpSession session, String message, String messageType) {
        session.setAttribute("messageBody", message);
        session.setAttribute("messageType", messageType);
    }
    
    public void setMessageOnModel(Model model, String messageBody, String messageType) {
        model.addAttribute("messageBody", messageBody);
        model.addAttribute("messageType", messageType);
    }
    
}
