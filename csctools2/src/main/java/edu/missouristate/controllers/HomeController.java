package edu.missouristate.controllers;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
	
	@Autowired
	BaseController baseController;
	
    @GetMapping("/")
    public String getIndex() {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	
    	if (auth != null && auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_ADMIN"))) {
    		return "index";
    	} else if (auth != null && auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_STUDENT"))) {
    		return "index";
    	}

        return "redirect:/login";
    }

	@GetMapping("/error")
	public String getError(final HttpServletResponse response, HttpSession session, 
			Model model, Exception ex) {
		
		baseController.handleException(ex, response, session, model);
		return "error";
	}
	
}
