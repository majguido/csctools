package edu.missouristate.controllers;

import java.util.Arrays;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import edu.missouristate.dao.UserRepository;
import edu.missouristate.domain.User;
import edu.missouristate.util.helpers.Helper;
import edu.missouristate.util.helpers.SecurityHelper;

@Controller
public class LoginController {
	
	@Autowired
	UserRepository userRepository;
	
	@GetMapping("/login")
	public String getLogin() {
		return "login";
	}
	
    @GetMapping("/dashboard")
    public String getDashboard(HttpSession session){
    	String uri = (String)session.getAttribute("uri");
    	SecurityContext securityContext = SecurityContextHolder.getContext();
    	boolean hasAuthority = SecurityHelper.hasAnyAuthority(securityContext, 
    				Arrays.asList(new String[] {"ROLE_ADMIN","ROLE_STUDENT"}));
    	
    	if (hasAuthority) {
    		String username = SecurityHelper.getUsername(securityContext);
    		
    		if (Helper.hasText(username)) {
    			User user = userRepository.findByEmail(username);
    			
    			if (user != null) {
    				session.setAttribute("user", user);
    			}
    		}

    		if (Helper.hasText(uri)) {
    			session.removeAttribute("uri");
    			return "redirect:" + uri;	
    		}
    		
    		return "redirect:/";
    	}

        return "redirect:/login";
    }
}
