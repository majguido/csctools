package edu.missouristate.controllers.students;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import edu.missouristate.domain.CourseRequest;
import edu.missouristate.service.CourseService;
import edu.missouristate.util.helpers.Helper;

@Controller
public class CourseRequestController {

	@Autowired
	CourseService courseService;
	
	@GetMapping("/students/courserequests") 
	public String getCourseRequest(Model model, HttpSession session,
			@ModelAttribute("message") String message, 
			@ModelAttribute("messageType") String messageType) {
		
		courseService.prepareCourseRequests(session, model, null);
		return "courserequests";
	}
	
	@PostMapping("/students/courserequests") 
	public String postCourseRequest(@ModelAttribute("courserequest") CourseRequest courseRequest,
			BindingResult result, RedirectAttributes ra, Model model, HttpSession session) {

		courseService.validateCourseRequestForm(courseRequest, result);
		
		// Process Form Error
		if (result.hasErrors()) {
			//Helper.addMessage(model, "success", "Course Request Saved Successfully");
			courseService.prepareCourseRequests(session, model, courseRequest);	
            return "courserequests";
        }
		
		// Process Good Form
		courseService.saveCourseRequest(courseRequest);
		Helper.addRedirectAttributeMessage(ra, "success", "Course Request Saved Successfully");
		return "redirect:/students/courserequests";
	}
	
	
}
