package edu.missouristate.config;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;

import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;

import edu.missouristate.handlers.InterceptorHandler;
import edu.missouristate.util.JsonUtil;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "edu.missouristate")
//@Import({ SecurityConfigOLD2.class })
public class WebMvcConfig implements WebMvcConfigurer {
	
	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		converters.add(mappingJackson2HttpMessageConverter());
	}

	@Bean
	public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
		MappingJackson2HttpMessageConverter props = new MappingJackson2HttpMessageConverter();

		/*
		 * Registering a hibernate module to the JsonUtil Object Mapper makes the Object
		 * Mapper Hibernate Aware
		 */
		JsonUtil.registerModule(new Hibernate5Module());

		// copy of JsonUtil's objectMapper instance
		props.setObjectMapper(JsonUtil.copyOfObjectMapper());
		return props;
	}
	
//    @Controller
//    public static class FaviconController {
//        @RequestMapping({"favicon.ico", "/favicon.ico", "/resources/img/ico/favicon.ico"})
//        public String favicon(HttpServletRequest req) {
//        	//return "redirect:/resources/img/ico/favicon.ico";
//        	String contextPath = req.getContextPath();
//        	return contextPath + "/resources/img/ico/favicon.ico";
//        }
//    }
    
	@Bean(name="Views")
	public ViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}

    @Bean
    public TilesConfigurer tilesConfigurer() {
        TilesConfigurer tilesConfigurer = new TilesConfigurer();
        tilesConfigurer.setDefinitions(
          new String[] { "/WEB-INF/views/**/tiles.xml" });
        tilesConfigurer.setCheckRefresh(true);
         
        return tilesConfigurer;
    }
    
    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        TilesViewResolver viewResolver = new TilesViewResolver();
        registry.viewResolver(viewResolver);
    }
     
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
    }
    
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new InterceptorHandler());
	}
	
}