package edu.missouristate.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.missouristate.domain.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByRole(String name);
}
