package edu.missouristate.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.missouristate.domain.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);
}
