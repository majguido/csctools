package edu.missouristate.dao.custom;

import java.util.Map;

public interface CourseRepositoryCustom {
	public Map<Integer, String> getCourseMapByStatus(Integer statusId);
}