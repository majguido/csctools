package edu.missouristate.dao;

import java.util.List;
import java.util.Map;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.missouristate.dao.custom.CourseRepositoryCustom;
import edu.missouristate.domain.Course;

@Repository
public interface CourseRepository extends CrudRepository<Course, Integer>, CourseRepositoryCustom {
    List<Course> findByStatusId(Integer statusId);

	
}
