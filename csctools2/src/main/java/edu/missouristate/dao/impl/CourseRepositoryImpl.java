package edu.missouristate.dao.impl;

import java.util.Map;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import com.querydsl.core.group.GroupBy;

import edu.missouristate.dao.custom.CourseRepositoryCustom;
import edu.missouristate.domain.Course;
import edu.missouristate.domain.QCourse;

public class CourseRepositoryImpl extends QuerydslRepositorySupport implements CourseRepositoryCustom {

	QCourse courseTable = QCourse.course;
	
    public CourseRepositoryImpl() {
        super(Course.class);
    }

	@Override
	public Map<Integer, String> getCourseMapByStatus(Integer statusId) {
		return from(courseTable)
				.where(courseTable.statusId.eq(statusId))
				.transform(GroupBy.groupBy(courseTable.courseId)
						.as(courseTable.courseSection
								.concat(" ")
								.concat(courseTable.courseTitle)));
	}

    

}